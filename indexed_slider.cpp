#include "indexed_slider.hpp"

kaos::indexed_slider::indexed_slider(int i, QWidget *parent) : QSlider(parent) {
  id = i;
}

kaos::indexed_slider::indexed_slider(int i, Qt::Orientation o, QWidget *parent) : QSlider(o, parent) {
  id = i;
}

void kaos::indexed_slider::sliderChange(SliderChange c) {
  QSlider::sliderChange(c);
  if (c == QAbstractSlider::SliderValueChange) {
    emit value_changed(value(), id);
  }
}
