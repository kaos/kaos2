/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fractal_image.hpp"

#include <iostream>

using namespace std;
using namespace kaos::fractal;

image::image(QWidget *parent) : QLabel(parent) {
  selection_started = false;
  record_path = false;
  buf = 0;
  xmin = 0;
  xmax = 0;
  ymin = 0;
  ymax = 0;
}

void image::paintEvent(QPaintEvent *e) {
  QPainter qp;
  int axis_buf = 10;
  int hmargin = 20;
  int vmargin = 40;
  int tmargin = 2;
  int hoffset = vmargin + axis_buf;
  int voffset = hmargin + axis_buf;
  int aw = 4;
  int textheight = hmargin - 8;
  int textwidth = vmargin - aw - tmargin;
  int wlabel = 30;
  int i;
  int nlines = 7;
  int w = width();
  int h = height();

  QLabel::paintEvent(e);

  qp.begin(this);
  qp.setFont(QFont(qp.font().family(), (2 * textheight) / 3));
  qp.setCompositionMode(QPainter::CompositionMode_Difference);
  qp.setPen(QColor(0xFF, 0xFF, 0xFF));

  if (buf) {
    // draw image inverts colors
    // qp.drawImage(0, 0, *buf);

    // selection rectangle
    if (selection_started) {
      qp.drawRect(selection_rect);
    }

    // axis
    qp.drawLine(vmargin, axis_buf, vmargin, h - voffset);
    qp.drawLine(hoffset, h - hmargin, w - axis_buf, h - hmargin);

    for (i = 0; i < nlines; i++) {
      // horizontal
      int xpos = hoffset + (i * (w - hoffset - axis_buf)) / (nlines - 1);
      int ypos = h - hmargin;
      qp.drawLine(xpos, ypos - aw, xpos, ypos + aw);
      qp.drawText(xpos - wlabel / 2, ypos + aw, textwidth, textheight, Qt::AlignJustify, QString::number(xmin + xpos * (xmax - xmin) / w, 'g', 3));

      // vertical
      ypos = axis_buf + (i * (h - voffset - axis_buf)) / (nlines - 1);
      xpos = vmargin;
      qp.drawLine(xpos - aw, ypos, xpos + aw, ypos);
      qp.drawText(tmargin, ypos - (hmargin - aw) / 2, textwidth, textheight, Qt::AlignJustify, QString::number(ymin + (h - ypos) * (ymax - ymin) / h, 'g', 3));
    }
  }

  qp.end();
}

vector<double> image::mouse2coords() {
  QPoint p = mapFromGlobal(QCursor::pos());
  double x1 = xmin + p.x() * (xmax - xmin) / (double)width();
  double x2 = ymin + p.y() * (ymax - ymin) / (double)height();

  return {x1, x2};
}

void image::mousePressEvent(QMouseEvent *e) {
  if (record_path) {
    e->ignore();
  } else {
    selection_rect.setTopLeft(e->pos());
    selection_rect.setBottomRight(e->pos());
    selection_started = true;
    e->accept();
  }
}

void image::mouseMoveEvent(QMouseEvent *e) {
  if (record_path && e->buttons() == Qt::LeftButton) {
    animation_path.push_back(mouse2coords());
    e->accept();
  } else if (selection_started) {
    selection_rect.setBottomRight(e->pos());
    repaint();
    e->accept();
  } else {
    e->ignore();
  }
}

void image::mouseReleaseEvent(QMouseEvent *e) {
  if (record_path) {
    emit path_created(animation_path);
    record_path = false;
    animation_path.clear();
    e->accept();
  } else if (selection_started) {
    selection_started = false;
    emit selected(selection_rect);
    e->accept();
  } else {
    e->ignore();
  }
}

void image::update_bounds(double *b) {
  xmin = b[0];
  xmax = b[1];
  ymin = b[2];
  ymax = b[3];
}

void image::set_image(QImage *b) {
  if (buf = b) {
    setPixmap(QPixmap::fromImage(*b));
  } else {
    setPixmap(QPixmap());
  }

  update();
}
