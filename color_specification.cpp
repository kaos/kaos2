#include "color_specification.hpp"

using namespace kaos;

QDataStream &fractal::operator<<(QDataStream &out, const fractal::color_specification &spec) {
  return out << spec.interpolators;
}

QDataStream &fractal::operator>>(QDataStream &in, fractal::color_specification &spec) {
  return in >> spec.interpolators;
}
