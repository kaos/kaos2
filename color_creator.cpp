/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "color_creator.hpp"

#include <iostream>

#include "colormap.hpp"
#include "property.hpp"

using namespace std;
using namespace kaos;
using namespace interface;

color_node::color_node(types::value v, QColor c, QWidget *parent) : QWidget(parent) {
  color = c;
  value = v;

  setLayout(new QVBoxLayout());
  layout()->addWidget(ivalue = new QLineEdit(QString::number(value), this));
  layout()->addWidget(icolor = new QPushButton("Color", this));
  connect(icolor, SIGNAL(clicked()), this, SLOT(set_color()));
  connect(ivalue, SIGNAL(textChanged(QString)), this, SLOT(set_value(QString)));
  color::set_widget_background(icolor, color);
}

void color_node::set_value(QString v) {
  value = v.toDouble();
}

void color_node::set_color() {
  QColorDialog *d = new QColorDialog;
  d->setOption(QColorDialog::ShowAlphaChannel);
  if (d->exec()) {
    color = d->currentColor();
    color::set_widget_background(icolor, color);
  }
  delete d;
}

bool color_node::compare::operator()(color_node *a, color_node *b) {
  return a->value < b->value;
}

interface::interpolation_gui::interpolation_gui(color::interpolation c, QWidget *parent) : QWidget(parent) {
  setLayout(new QHBoxLayout());

  merge = 0;

  // combo box for selecting property
  select_property = new QComboBox(this);
  select_property->addItem(c.value_map);
  select_property->addItems(property::string_list());
  layout()->addWidget(select_property);

  // button for adding a new node
  QPushButton *b_addnode = new QPushButton("add node", this);
  connect(b_addnode, SIGNAL(clicked()), this, SLOT(add_node()));
  layout()->addWidget(b_addnode);

  // nodes
  for (auto x : c.nodes) {
    color_node *cn = new color_node(x.first, x.second);
    nodes.push_back(cn);
    closable *n = new closable(cn, this);
    connect(n, SIGNAL(was_closed(closable *)), this, SLOT(close_node(closable *)));
    layout()->addWidget(n);
  }

  // apply dims
  for (int i = 0; i < c.apply_dims.size(); i++) {
    QCheckBox *cb = new QCheckBox("dim " + QString::number(i), this);
    cb->setChecked(c.apply_dims[i]);
    apply_dims.push_back(cb);
    layout()->addWidget(cb);
  }
}

void interpolation_gui::add_node() {
  color_node *cn = new color_node(0, QColor());
  nodes.push_back(cn);
  closable *n = new closable(cn, this);
  connect(n, SIGNAL(was_closed(closable *)), this, SLOT(close_node(closable *)));
  layout()->addWidget(n);
}

void interpolation_gui::close_node(closable *n) {
  color_node *c = qobject_cast<color_node *>(n->content);
  if (c) {
    nodes.removeAll(c);
  } else {
    cout << "interpolation_gui::close_node: cast failed!" << endl;
  }

  delete n;
}

color_creator::color_creator(fractal::color_specification s, int d) : QDialog(), dim(d) {
  setLayout(new QVBoxLayout());

  QPushButton *b_new = new QPushButton("Add colormap", this);
  connect(b_new, SIGNAL(clicked()), this, SLOT(add_map()));
  layout()->addWidget(b_new);

  QPushButton *b_accept = new QPushButton("Accept", this);
  connect(b_accept, SIGNAL(clicked()), this, SLOT(accept()));
  layout()->addWidget(b_accept);

  for (auto x : s.interpolators) {
    add_map(x.first, (color::merge_operator)x.second);
  }

  layout()->setSizeConstraint(QLayout::SetFixedSize);
}

void color_creator::add_map() {
  add_map(color::interpolation(dim), color::merge_add);
}

void color_creator::add_map(color::interpolation s, color::merge_operator m) {
  interpolation_gui *g = new interpolation_gui(s, this);
  closable *cg = new closable(g, this);
  connect(cg, SIGNAL(was_closed(closable *)), this, SLOT(close_map(closable *)));

  if (!maps.empty()) {
    g->merge = new QComboBox();
    g->merge->addItems(color::merge_oplist());
    // todo: make a better solution to this
    g->merge->setCurrentIndex(color::merge_index(m));
    layout()->addWidget(g->merge);
  }

  maps.push_back(g);
  layout()->addWidget(cg);
}

void color_creator::close_map(closable *cg) {
  layout()->removeWidget(cg);
  interpolation_gui *ig = qobject_cast<interpolation_gui *>(cg->content);

  if (ig) {
    maps.removeAll(ig);
    if (ig->merge) {
      layout()->removeWidget(ig->merge);
      delete ig->merge;
    }
  } else {
    cout << "color_creator::close_map: cast failed!" << endl;
  }

  delete cg;
}

fractal::color_specification color_creator::parse() {
  fractal::color_specification s;
  int i, j;

  s.interpolators.resize(maps.size());
  i = 0;
  for (auto x = maps.begin(); x != maps.end(); x++) {
    color::interpolation c;
    auto m = *x;

    // value map
    c.value_map = m->select_property->currentText();
    c.nodes.resize(m->nodes.size());

    // nodes
    qSort(m->nodes.begin(), m->nodes.end(), interface::color_node::compare());
    j = 0;
    for (auto y = m->nodes.begin(); y != m->nodes.end(); y++) {
      c.nodes[j].first = (*y)->value;
      c.nodes[j].second = (*y)->color;
      j++;
    }

    // apply dims
    c.apply_dims.clear();
    for (auto y : m->apply_dims) {
      c.apply_dims.push_back(y->isChecked());
    }

    s.interpolators[i].first = c;
    s.interpolators[i].second = m->merge ? color::merge_opmap[m->merge->currentText()] : color::merge_overwrite;
    i++;
  }

  return s;
}

// todo: move from constructor..?
// void color_creator::deparse(fractal::color_specification s){

// }
