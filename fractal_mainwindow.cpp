#include "fractal_mainwindow.hpp"

#include <QtWidgets>

#include "fractal_templates.hpp"

using namespace std;
using namespace kaos;
using namespace interface;

fractal_mainwindow::fractal_mainwindow(QWidget *p, Qt::WindowFlags f) : QMainWindow(p, f) {
  // build main interface
  fractal_navigator *iface = new fractal_navigator(this);

  // shortcuts
  new QShortcut(QKeySequence(Qt::Key_J), iface, SLOT(julia_from_mouse()));
  new QShortcut(QKeySequence(Qt::Key_N), iface, SLOT(new_fractal()));

  // actions and menus

  // main menu
  QMenu *main_menu = menuBar()->addMenu("Menu");

  // new fractal menu
  QMenu *new_menu = main_menu->addMenu("New fractal");
  QActionGroup *template_group = new QActionGroup(this);
  connect(template_group, SIGNAL(triggered(QAction *)), iface, SLOT(template_selected(QAction *)));

  for (auto x : fractal::template_list()) {
    QAction *t = new QAction(this);
    t->setText(x);
    template_group->addAction(t);
    new_menu->addAction(t);
  }

  QAction *save_action = new QAction(this);
  connect(save_action, SIGNAL(triggered()), iface, SLOT(save_current()));
  save_action->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_S));
  save_action->setText("Save");

  QAction *load_action = new QAction(this);
  connect(load_action, SIGNAL(triggered()), iface, SLOT(load_file()));
  load_action->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_O));
  load_action->setText("Load from file");

  QAction *export_action = new QAction(this);
  connect(export_action, SIGNAL(triggered()), iface, SLOT(export_current()));
  export_action->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_X));
  export_action->setText("Export");

  QAction *video_action = new QAction(this);
  connect(video_action, SIGNAL(triggered()), iface, SLOT(generate_video_ab()));
  video_action->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_V));
  video_action->setText("Generate video");

  QAction *copy_action = new QAction(this);
  connect(copy_action, SIGNAL(triggered()), iface, SLOT(copy_current()));
  copy_action->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_C));
  copy_action->setText("Copy current");

  QAction *delete_action = new QAction(this);
  connect(delete_action, SIGNAL(triggered()), iface, SLOT(delete_current()));
  delete_action->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_D));
  delete_action->setText("Delete current");

  QAction *path_action = new QAction(this);
  connect(path_action, SIGNAL(triggered()), iface, SLOT(set_record_path()));
  path_action->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_P));
  path_action->setText("Record animation path");

  QAction *quit_action = new QAction(this);
  connect(quit_action, SIGNAL(triggered()), qApp, SLOT(quit()));
  quit_action->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Q));
  quit_action->setText("Quit");

  main_menu->addAction(save_action);
  main_menu->addAction(load_action);
  main_menu->addAction(export_action);
  main_menu->addAction(video_action);
  main_menu->addAction(copy_action);
  main_menu->addAction(delete_action);
  main_menu->addAction(path_action);
  main_menu->addSeparator();
  main_menu->addAction(quit_action);

  // navigate menu
  QMenu *navigate_menu = menuBar()->addMenu("Navigate");

  QAction *zoomin_action = new QAction(this);
  connect(zoomin_action, SIGNAL(triggered()), iface, SLOT(zoom_in()));
  zoomin_action->setShortcut(QKeySequence(Qt::Key_Plus));
  zoomin_action->setText("Zoom in");

  QAction *zoomout_action = new QAction(this);
  connect(zoomout_action, SIGNAL(triggered()), iface, SLOT(zoom_out()));
  zoomout_action->setShortcut(QKeySequence(Qt::Key_Minus));
  zoomout_action->setText("Zoom out");

  QAction *tleft_action = new QAction(this);
  connect(tleft_action, SIGNAL(triggered()), iface, SLOT(translate_left()));
  tleft_action->setShortcut(QKeySequence(Qt::Key_Left));
  tleft_action->setText("Translate left");

  QAction *tright_action = new QAction(this);
  connect(tright_action, SIGNAL(triggered()), iface, SLOT(translate_right()));
  tright_action->setShortcut(QKeySequence(Qt::Key_Right));
  tright_action->setText("Translate right");

  QAction *tup_action = new QAction(this);
  connect(tup_action, SIGNAL(triggered()), iface, SLOT(translate_up()));
  tup_action->setShortcut(QKeySequence(Qt::Key_Up));
  tup_action->setText("Translate up");

  QAction *tdown_action = new QAction(this);
  connect(tdown_action, SIGNAL(triggered()), iface, SLOT(translate_down()));
  tdown_action->setShortcut(QKeySequence(Qt::Key_Down));
  tdown_action->setText("Translate down");

  QAction *update_action = new QAction(this);
  connect(update_action, SIGNAL(triggered()), iface, SLOT(update_fractal()));
  update_action->setShortcut(QKeySequence(Qt::Key_Return));
  update_action->setText("Update fractal");

  navigate_menu->addAction(zoomin_action);
  navigate_menu->addAction(zoomout_action);
  navigate_menu->addAction(tleft_action);
  navigate_menu->addAction(tright_action);
  navigate_menu->addAction(tup_action);
  navigate_menu->addAction(tdown_action);
  navigate_menu->addSeparator();
  navigate_menu->addAction(update_action);

  // edit menu
  QMenu *edit_menu = menuBar()->addMenu("Edit");

  QAction *edit_system_action = new QAction(this);
  connect(edit_system_action, SIGNAL(triggered()), iface, SLOT(edit_system()));
  edit_system_action->setShortcut(QKeySequence(Qt::ALT + Qt::Key_S));
  edit_system_action->setText("System");
  edit_menu->addAction(edit_system_action);

  QAction *edit_bounds_action = new QAction(this);
  connect(edit_bounds_action, SIGNAL(triggered()), iface, SLOT(edit_bounds()));
  edit_bounds_action->setShortcut(QKeySequence(Qt::ALT + Qt::Key_B));
  edit_bounds_action->setText("Bounds");
  edit_menu->addAction(edit_bounds_action);

  QAction *edit_view_action = new QAction(this);
  connect(edit_view_action, SIGNAL(triggered()), iface, SLOT(edit_view()));
  edit_view_action->setShortcut(QKeySequence(Qt::ALT + Qt::Key_V));
  edit_view_action->setText("View");
  edit_menu->addAction(edit_view_action);

  QAction *edit_color_action = new QAction(this);
  connect(edit_color_action, SIGNAL(triggered()), iface, SLOT(edit_color()));
  edit_color_action->setShortcut(QKeySequence(Qt::ALT + Qt::Key_C));
  edit_color_action->setText("Color");
  edit_menu->addAction(edit_color_action);

  // visual menu
  QMenu *visual_menu = menuBar()->addMenu("Visual");

  QAction *resolution_action = new QAction(this);
  connect(resolution_action, SIGNAL(triggered()), iface, SLOT(edit_resolution()));
  resolution_action->setShortcut(QKeySequence(Qt::ALT + Qt::Key_R));
  resolution_action->setText("Image resolution");
  visual_menu->addAction(resolution_action);

  QAction *res2image_action = new QAction(this);
  connect(res2image_action, SIGNAL(triggered()), iface, SLOT(resolution_to_image_size()));
  res2image_action->setText("Res. to image size");
  visual_menu->addAction(res2image_action);

  // add interface to mw
  setCentralWidget(iface);

  // start interface
  showMaximized();

  // gain focus
  setFocus(Qt::ActiveWindowFocusReason);
}
