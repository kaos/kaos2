/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fractal_lwi.hpp"

#include <iostream>

using namespace std;
using namespace kaos;
using namespace interface;

fractal_lwi::fractal_lwi(fractal::specification s, QListWidget *lw) : QListWidgetItem(lw), spec(s) {
  block_cleanup = false;
}

fractal_lwi::~fractal_lwi() {
  if (!block_cleanup) spec.cleanup();
}

void fractal_lwi::update_size(QSize s) {
  image = QImage(s, QImage::Format_ARGB32);
}

QImage *fractal_lwi::get_image() {
  return &image;
}

void fractal_lwi::update_icon() {
  setIcon(QIcon(QPixmap::fromImage(image)));
}
