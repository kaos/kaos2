/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once

#include <list>
#include <vector>

namespace kaos {
namespace fractal {
class system_specification;
class view_specification;
class bounds_specification;
class specification;
};  // namespace fractal
namespace types {
/*! type used to define value */
typedef double value;

struct it_point {
  value x;
  value x2;
};

/*! \brief struct used to store point iteration data */
struct result {
  int dimension;
  int iterations;
  int max_iterations;

  std::vector<std::vector<it_point>> data;
  std::vector<value> get_row(int j) const;
  std::vector<value> get_row2(int j) const;
};
/*! point iteration function type */
typedef result (*compute_point)(std::vector<double> x, std::vector<double> c, int max_its);
};  // namespace types
};  // namespace kaos
