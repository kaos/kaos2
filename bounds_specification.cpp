#include "bounds_specification.hpp"

using namespace kaos;

fractal::bounds_specification::bounds_specification() {
  bounds.resize(4);
}

QDataStream &fractal::operator<<(QDataStream &out, const fractal::bounds_specification &spec) {
  return out << spec.bounds << spec.base[0] << spec.base[1];
}

QDataStream &fractal::operator>>(QDataStream &in, fractal::bounds_specification &spec) {
  return in >> spec.bounds >> spec.base[0] >> spec.base[1];
}
