/* The MIT License (MIT)

   Copyright (c) 2014 Ross Linscott

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
   OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
   WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fractal_navigator.hpp"

#include <unistd.h>

#include <QtConcurrent>
#include <QtWidgets>
#include <cmath>
#include <iostream>
#include <utility>
#include <chrono>

#include "colormap.hpp"
#include "elementary.hpp"
#include "fractal_image.hpp"
#include "fractal_lw.hpp"
#include "fractal_lwi.hpp"
#include "fractal_specification.hpp"
#include "fractal_templates.hpp"

// dialogs
#include "bounds_creator.hpp"
#include "color_creator.hpp"
#include "export_image_interface.hpp"
#include "generate_video_interface.hpp"
#include "system_creator.hpp"
#include "view_creator.hpp"

using namespace kaos;
using namespace interface;
using namespace fractal;
using namespace std;

specification julia_inversion(specification spec, double x1, double x2) {
  specification res = spec;

  res.bounds.bounds[0] = -2;
  res.bounds.bounds[1] = 2;
  res.bounds.bounds[2] = -2;
  res.bounds.bounds[3] = 2;

  vector<fractal::dimtype> nonview_dims = spec.nonview_dims();

  res.view.horizontal = nonview_dims[0];
  res.view.vertical = nonview_dims[1];

  nonview_dims = res.nonview_dims();

  res.set_dimension_value(spec.view.horizontal, x1);
  res.set_dimension_value(spec.view.vertical, x2);

  return res;
}

fractal_navigator::fractal_navigator(QWidget *parent) : QWidget(parent) {
  // configuration
  resolution = QSize(400, 400);
  worker_fractal = 0;

  // controls section
  QVBoxLayout *controls_layout = new QVBoxLayout();

  contents_widget = new fractal_lw(this);
  connect(contents_widget, SIGNAL(change(fractal_lwi *, fractal_lwi *)), this, SLOT(change_page(fractal_lwi *, fractal_lwi *)));

  contents_widget->setMovement(QListView::Static);
  contents_widget->setMaximumWidth(300);
  contents_widget->setMinimumWidth(200);
  contents_widget->setMinimumHeight(40);
  contents_widget->setSpacing(12);
  contents_widget->setIconSize(QSize(30, 30));

  QPushButton *b_system = new QPushButton("system", this);
  QPushButton *b_bounds = new QPushButton("bounds", this);
  QPushButton *b_view = new QPushButton("view", this);
  QPushButton *b_color = new QPushButton("color", this);

  connect(b_system, SIGNAL(clicked()), this, SLOT(edit_system()));
  connect(b_bounds, SIGNAL(clicked()), this, SLOT(edit_bounds()));
  connect(b_view, SIGNAL(clicked()), this, SLOT(edit_view()));
  connect(b_color, SIGNAL(clicked()), this, SLOT(edit_color()));

  controls_layout->addWidget(contents_widget);
  controls_layout->addWidget(b_system);
  controls_layout->addWidget(b_bounds);
  controls_layout->addWidget(b_view);
  controls_layout->addWidget(b_color);

  QPushButton *b_update = new QPushButton("update", this);
  connect(b_update, SIGNAL(clicked()), this, SLOT(update_fractal()));
  controls_layout->addWidget(b_update);

  // image section
  image_label = new image(this);
  connect(image_label, &fractal::image::selected, this, &fractal_navigator::zoom);
  connect(image_label, &fractal::image::path_created, this, &fractal_navigator::generate_video_path);

  // image :: progress bar
  QProgressBar *progress_bar = new QProgressBar(this);
  progress_bar->setMinimum(0);
  progress_bar->setMaximum(resolution.width() * resolution.height());

  // image :: layout
  QVBoxLayout *view_layout = new QVBoxLayout();
  view_layout->addWidget(image_label);
  view_layout->addWidget(progress_bar);

  // computations
  connect(&fwatch, SIGNAL(finished()), this, SLOT(reload_data()));
  connect(&fwatch, SIGNAL(progressValueChanged(int)), progress_bar, SLOT(setValue(int)));
  connect(&fwatch, SIGNAL(canceled()), this, SLOT(unload_data()));

  // main layout
  QHBoxLayout *main_layout = new QHBoxLayout();
  main_layout->addLayout(controls_layout);
  main_layout->addLayout(view_layout);

  setLayout(main_layout);
}

void fractal_navigator::change_page(fractal_lwi *current, fractal_lwi *previous) {
  cout << "change page" << endl;
  specification *s;

  if (!current) {
    image_label->set_image(0);
    return;
  }

  s = &(current->spec);

  if (s->dl_handle || !s->require_compile) {
    cout << "change page: spec already init" << endl;
  }

  image_label->update_bounds(&(s->bounds.bounds[0]));
  image_label->set_image(current->get_image());
}

void fractal_navigator::set_record_path() {
  image_label->record_path = true;
}

void fractal_navigator::delete_current() {
  fractal_lwi *lwi = contents_widget->current_item();

  if (lwi && lwi != worker_fractal) {
    contents_widget->removeItemWidget(lwi);
    delete lwi;
    cout << "deleted current" << endl;
  }

  // todo: does this trigger change_page?
}

void fractal_navigator::template_selected(QAction *a) {
  template_selected(a->text());
}

void fractal_navigator::template_selected(QString s) {
  if (fractal::templates.count(s) > 0) {
    add_fractal(fractal::templates[s]);
  } else {
    cout << "fractal_navigator::template_selected: not found: " << q2cstring(s) << endl;
  }
}

void fractal_navigator::copy_current() {
  specification *s = contents_widget->get_spec();

  if (s) {
    add_fractal(*s);
  }
}

void fractal_navigator::new_fractal() {
  template_selected(template_list().front());
}

void fractal_navigator::add_fractal(fractal::specification s) {
  s.dl_handle = 0;
  s.require_compile = true;
  s.libname = "";
  fractal_lwi *lwi = new fractal_lwi(s);
  lwi->setText(s.label);
  lwi->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
  contents_widget->addItem(lwi);
  contents_widget->setCurrentRow(contents_widget->count() - 1);
}

void fractal_navigator::zoom_in() {
  static_zoom(2);
}

void fractal_navigator::zoom_out() {
  static_zoom(0.5);
}

void fractal_navigator::translate_up() {
  translate(0, -1);
}

void fractal_navigator::translate_down() {
  translate(0, 1);
}

void fractal_navigator::translate_left() {
  translate(-1, 0);
}

void fractal_navigator::translate_right() {
  translate(1, 0);
}

void fractal_navigator::edit_system() {
  specification *spec = contents_widget->get_spec();
  if (!spec) return;

  system_creator *s = new system_creator(spec->system);
  if (s->exec()) {
    spec->system = s->parse();
    spec->bounds.base[fractal::view_state].resize(spec->system.update_expression.size());
    spec->bounds.base[fractal::view_param].resize(spec->system.parse_num_params());
    spec->view.trim(spec->bounds.base[fractal::view_state].size(), spec->bounds.base[fractal::view_param].size());

    QList<bool> upd_dims;
    for (int i = 0; i < spec->system.update_expression.size(); i++) upd_dims.push_back(false);
    upd_dims[0] = true;

    for (auto &x : spec->color.interpolators) {
      if (x.first.apply_dims.size() != upd_dims.size()) x.first.apply_dims = upd_dims;
    }

    spec->require_compile = true;
  }
  delete s;
}

void fractal_navigator::edit_bounds() {
  specification *spec = contents_widget->get_spec();
  if (!spec) return;

  bounds_creator *s = new bounds_creator(spec->bounds);
  if (s->exec()) {
    spec->bounds = s->parse();
    image_label->update_bounds(&(spec->bounds.bounds[0]));
  }
  delete s;
}

void fractal_navigator::edit_view() {
  specification *spec = contents_widget->get_spec();
  if (!spec) return;

  view_creator *s = new view_creator(spec->view, spec->bounds.base[fractal::view_state].size(), spec->bounds.base[fractal::view_param].size());
  if (s->exec()) {
    spec->view = s->parse();
  }
  delete s;
}

void fractal_navigator::edit_color() {
  specification *spec = contents_widget->get_spec();
  if (!spec) return;
  color_creator *s = new color_creator(spec->color, spec->dimension());
  if (s->exec()) {
    spec->color = s->parse();
  }
  delete s;
}

void fractal_navigator::edit_resolution() {
  QMessageBox d(QMessageBox::Question, "Image resolution", "Select image resolution:", QMessageBox::Ok);
  QLineEdit *w = new QLineEdit();
  QLineEdit *h = new QLineEdit();

  w->setText(QString::number(resolution.width()));
  h->setText(QString::number(resolution.height()));

  d.layout()->addWidget(new QLabel("width:"));
  d.layout()->addWidget(w);
  d.layout()->addWidget(new QLabel("height:"));
  d.layout()->addWidget(h);

  if (d.exec()) {
    QSize rtemp = QSize(w->text().toInt(), h->text().toInt());
    if (rtemp.width() > 0 && rtemp.height() > 0) {
      resolution = rtemp;
      cout << "set resolution to: " << resolution.width() << "x" << resolution.height() << endl;
      update_fractal();
    }
  }
}

void fractal_navigator::resolution_to_image_size() {
  resolution = image_label->size();
  update_fractal();
}

void fractal_navigator::update_fractal() {
  set_worker_spec();
  if (!worker_fractal) return;
  specification *spec = &worker_fractal->spec;

  cout << "update fractal" << endl;
  if ((!spec->require_compile) || spec->compile()) {
    spec->require_compile = false;
    update_jobs(spec);
    start_compute();
  } else {
    cout << "error: failed to compile fractal" << endl;
    exit(-1);
  }
}

void fractal_navigator::update_jobs(specification *spec) {
  if (!spec) return;
  job jbuf;

  call_points.resize(resolution.width() * resolution.height());
  for (int i = 0; i < resolution.width(); i++) {
    for (int j = 0; j < resolution.height(); j++) {
      jbuf.spec = spec;
      jbuf.a = spec->bounds.bounds[0] + (spec->bounds.bounds[1] - spec->bounds.bounds[0]) * i / (double)resolution.width();
      jbuf.b = spec->bounds.bounds[2] + (spec->bounds.bounds[3] - spec->bounds.bounds[2]) * (resolution.height() - j - 1) / (double)resolution.height();
      call_points[i + j * resolution.width()] = jbuf;
    }
  }
  // image_buf = QImage(resolution.width(), resolution.height(), QImage::Format_ARGB32);
}

void fractal_navigator::start_compute() {
  fwatch.setFuture(QtConcurrent::mapped(call_points.begin(), call_points.end(), draw_pixel));
}

void fractal_navigator::unload_data() {
  image_label->set_image(0);
  worker_fractal = 0;
}

void fractal_navigator::reload_data() {
  cout << "Start reload data" << endl;
  auto t_start = chrono::steady_clock::now();
  if (!worker_fractal) {
    cout << "fractal_navigator::reload_data: no worker fractal!" << endl;
    return;
  }

  if (resolution.width() * resolution.height() != fwatch.future().results().size()) {
    cout << "fractal_navigator::reload_data: incorrect future size! is " << fwatch.future().results().size() << ", should be " << resolution.width() * resolution.height() << endl;
    return;
  }

  // first, unset image label's image
  image_label->set_image(0);

  int i;
  worker_fractal->update_size(image_label->size());
  QImage *ibuf = worker_fractal->get_image();
  int w = ibuf->width();
  int h = ibuf->height();
  QRgb *pix = (QRgb *)(ibuf->bits());
  QFuture<QRgb> const &f = fwatch.future();
  int n = w * h;
  float wr = resolution.width() / (float)w;
  float hr = resolution.height() / (float)h;

  // todo: allow alpha
  // build scaled image directly
  for (i = 0; i < n; i++) {
    int r = hr * (i / w) + 0.5;
    int c = wr * (i % w) + 0.5;
    r = r < resolution.height() ? r : resolution.height() - 1;
    c = c < resolution.width() ? c : resolution.width() - 1;
    pix[i] = f.resultAt(r * resolution.width() + c);
  }

  image_label->set_image(ibuf);
  worker_fractal->update_icon();
  update();
  worker_fractal = 0;
  auto t_end = chrono::steady_clock::now();

  cout << "Reload time: " << chrono::duration_cast<chrono::milliseconds>((t_end - t_start)).count() << endl;
}

void fractal_navigator::update_bounds(double xmin, double xmax, double ymin, double ymax) {
  specification *spec = contents_widget->get_spec();
  if (!spec) return;

  spec->bounds.bounds[0] = xmin;
  spec->bounds.bounds[1] = xmax;
  spec->bounds.bounds[2] = ymin;
  spec->bounds.bounds[3] = ymax;
  image_label->update_bounds(&(spec->bounds.bounds[0]));
}

void fractal_navigator::zoom(QRect r) {
  set_worker_spec();
  if (!worker_fractal) return;
  specification *spec = &worker_fractal->spec;

  int x = r.x();
  int y = r.y();
  int w = r.width();
  int h = r.height();

  if (!(w && h)) {
    update();
    return;
  }

  if (w < 0) {
    x += w;
    w = -w;
  }

  if (h < 0) {
    y += h;
    h = -h;
  }

  r = QRect(x, y, w, h);

  double dw = image_label->width();
  double dh = image_label->height();
  kaos::types::value xmin = spec->bounds.bounds[0];
  kaos::types::value xmax = spec->bounds.bounds[1];
  kaos::types::value ymin = spec->bounds.bounds[2];
  kaos::types::value ymax = spec->bounds.bounds[3];

  update_bounds(xmin + (xmax - xmin) * r.x() / dw,
                xmin + (xmax - xmin) * (r.x() + r.width()) / dw,
                ymin + (ymax - ymin) * (dh - (r.y() + r.height())) / dh,
                ymin + (ymax - ymin) * (dh - r.y()) / dh);

  update_jobs(spec);
  start_compute();
}

QRgb kaos::fractal::draw_pixel(job j) {
  try {
    if (!j.spec->color.interpolators.size()) {
      cout << "draw_pixel: no color interpolators!" << endl;
      return qRgb(0, 0, 0);
    }

    kaos::types::result res = j.spec->compute_point(j.a, j.b);
    auto interps = j.spec->color.interpolators;
    auto a = interps.begin();
    QColor c = a->first.compute_color(res);
    a++;

    for (; a != interps.end(); a++) {
      c = merge_colors(c, a->first.compute_color(res), (color::merge_operator)a->second);
    }

    return c.rgb();
  } catch (const std::exception& e) {
    cout << "Exception in draw_pixel: " << e.what() << endl;
    throw e;
  }
}

void fractal_navigator::translate(double x, double y) {
  set_worker_spec();
  if (!worker_fractal) return;
  specification *spec = &worker_fractal->spec;

  double dx = 0.5 * (spec->bounds.bounds[1] - spec->bounds.bounds[0]);
  double dy = 0.5 * (spec->bounds.bounds[3] - spec->bounds.bounds[2]);
  update_bounds(spec->bounds.bounds[0] + x * dx,
                spec->bounds.bounds[1] + x * dx,
                spec->bounds.bounds[2] + y * dy,
                spec->bounds.bounds[3] + y * dy);
  update_jobs(spec);
  start_compute();
}

void fractal_navigator::static_zoom(double a) {
  set_worker_spec();
  if (!worker_fractal) return;
  specification *spec = &worker_fractal->spec;

  double cx = (spec->bounds.bounds[0] + spec->bounds.bounds[1]) / 2;
  double cy = (spec->bounds.bounds[2] + spec->bounds.bounds[3]) / 2;
  double wx = spec->bounds.bounds[1] - spec->bounds.bounds[0];
  double wy = spec->bounds.bounds[3] - spec->bounds.bounds[2];

  update_bounds(cx - wx / (2 * a),
                cx + wx / (2 * a),
                cy - wy / (2 * a),
                cy + wy / (2 * a));
  update_jobs(spec);
  start_compute();
}

void fractal_navigator::save_current() {
  specification *spec = contents_widget->get_spec();
  if (!spec) {
    cout << "save_current: no fractal selected!" << endl;
    return;
  }

  QString fname = QFileDialog::getSaveFileName(this, "Save fractal");
  QFile f(fname);

  f.open(QIODevice::WriteOnly);
  QDataStream s(&f);
  s << (*spec);
  f.close();
  cout << "Saved to: " << fname.toStdString() << endl;
}

void fractal_navigator::load_file() {
  specification s;
  QString fname = QFileDialog::getOpenFileName(this, "Load fractal");
  QFile f(fname);

  f.open(QIODevice::ReadOnly);
  QDataStream ds(&f);
  ds >> s;
  f.close();
  cout << "loaded from: " << fname.toStdString() << endl;
  add_fractal(s);
}

bool fractal_navigator::generate_image(QString fname, fractal::specification spec, bool progress) {
  update_jobs(&spec);

  QProgressDialog *prog;
  if (progress) prog = new QProgressDialog(fname, "cancel", 0, resolution.width() * resolution.height());

  QFutureWatcher<QRgb> fw;

  if (progress) {
    connect(prog, SIGNAL(canceled()), &fw, SLOT(cancel()));
    connect(&fw, SIGNAL(progressValueChanged(int)), prog, SLOT(setValue(int)));
    prog->setModal(true);
    prog->show();
  }

  fw.setFuture(QtConcurrent::mapped(call_points.begin(), call_points.end(), draw_pixel));

  // wait for fw to finish, but meanwhile update the progressbar interface
  while (!fw.isFinished()) {
    usleep(500000);
    QCoreApplication::processEvents();
  }

  if (progress) delete prog;

  if (fw.isCanceled()) return false;

  if (fw.future().results().size() != resolution.width() * resolution.height()) {
    cout << "fractal_navigator::export_current(): invalid results size!" << endl;
    return false;
  }

  int i = 0;
  QImage image(resolution.width(), resolution.height(), QImage::Format_ARGB32);
  QRgb *pix = (QRgb *)image.bits();

  for (auto x : fw.future().results()) {
    pix[i++] = x | 0xff000000;
  }

  return image.save(fname);
}

void fractal_navigator::export_current() {
  set_worker_spec();
  if (!worker_fractal) return;
  specification *spec = &worker_fractal->spec;

  QSize buf_res = resolution;
  QString filename;

  imagex *e = new imagex();
  if (e->exec()) {
    int i;
    filename = QString("export/").append(e->get_filename());
    resolution = QSize(e->get_width(), e->get_height());
    generate_image(filename, *spec, true);

    // reset dimensions
    resolution = buf_res;
    update_jobs(spec);
  }

  worker_fractal = 0;
  delete e;
}

void fractal_navigator::generate_video_path(vector<vector<double>> animation_path) {
  set_worker_spec();
  if (!worker_fractal) return;

  if (animation_path.empty()) {
    cout << "No animation path data!" << endl;
    return;
  }

  videox *v = new videox(this, false);
  if (v->exec()) {
    specification spec = worker_fractal->spec;
    vector<specification> ss(animation_path.size());
    for (int i = 0; i < ss.size(); i++) ss[i] = julia_inversion(spec, animation_path[i][0], animation_path[i][1]);

    auto fgen = [this, ss](double r) {
      double t = r * ss.size();
      double bw = 2;
      vector<double> w(ss.size());
      for (int i = 0; i < ss.size(); i++) {
        w[i] = exp(-pow(t - i, 2) / pow(bw, 2));
      }
      return fractal::specification::weighted(w, ss);
    };

    generate_video(v, fgen);
  }
  delete v;
}

void fractal_navigator::generate_video_ab() {
  videox *v = new videox(this);
  if (v->exec()) {
    fractal::specification spec_a = v->get_f_source();
    fractal::specification spec_b = v->get_f_target();
    auto fgen = [spec_a, spec_b](double r) {
      return fractal::specification::between(spec_a, spec_b, r);
    };
    generate_video(v, fgen);
  }
  delete v;
}

// todo
void fractal_navigator::generate_video(videox *v, function<fractal::specification(double r)> fgen) {
  set_worker_spec();
  if (!worker_fractal) return;
  specification *orig_spec = &worker_fractal->spec;

  QSize buf_res = resolution;
  QString filename = v->get_filename();
  resolution = QSize(v->get_width(), v->get_height());
  double seconds = v->get_seconds();
  double fps = v->get_fps();
  int frames = seconds * fps;
  bool expslow = v->get_expslow();
  bool proceed = true;

  QProgressDialog *prog = new QProgressDialog("Generating video", "cancel", 0, frames);
  prog->setModal(true);
  prog->show();

  for (int i = 0; i < frames && proceed; i++) {
    double r = i / (double)(frames - 1);
    if (expslow) {
      r = log(1 + 100 * r) / log(101);
    }

    proceed = proceed && generate_image(("vidbuf/frame_" + to_string(i) + ".png").c_str(), fgen(r), false) && !prog->wasCanceled();
    prog->setValue(i);
  }

  delete prog;

  if (proceed) {
    // all images generated successfully
    string command = "./vidgen.sh " + filename.toStdString() + ".mp4 " + to_string(fps) + "&";
    system(command.c_str());
  }

  // reset dimensions
  resolution = buf_res;
  update_jobs(orig_spec);

  worker_fractal = 0;
}

// create a specification "inverting" the parameter/state
// visualisation configuration using the current mouse position (this
// requires exactly two parameter and two state dimensions)
void fractal_navigator::julia_from_mouse() {
  specification *spec = contents_widget->get_spec();
  if (!spec) return;

  vector<double> xs = image_label->mouse2coords();
  specification res = julia_inversion(*spec, xs[0], xs[1]);

  add_fractal(res);
}

fractal::specification *fractal_navigator::set_worker_spec() {
  if (worker_fractal) return 0;
  fractal_lwi *lwi = contents_widget->current_item();
  if (!lwi) return 0;
  fractal::specification *spec = &lwi->spec;
  if (!spec) return 0;
  worker_fractal = lwi;
  return spec;
}
