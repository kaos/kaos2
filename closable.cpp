#include "closable.hpp"

using namespace kaos;
using namespace interface;

closable::closable(QWidget *c, QWidget *p) : QFrame(p) {
  QHBoxLayout *l = new QHBoxLayout(this);

  QPushButton *bclose = new QPushButton("X", this);
  bclose->setObjectName("bclose");
  connect(bclose, SIGNAL(clicked()), this, SLOT(call_close()));

  content = c;
  content->setParent(this);
  l->addWidget(content);
  l->addWidget(bclose);

  setSizePolicy(QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum));
}

QSize closable::sizeHint() const {
  return content->sizeHint() + QSize(20, 0);
}

void closable::call_close() {
  emit was_closed(this);
}

// // supposedly nessecary for style to apply to QWidget subclass
// void closable::paintEvent(QPaintEvent *pe)
// {
//   QStyleOption o;
//   o.initFrom(this);
//   QPainter p(this);
//   style()->drawPrimitive(QStyle::PE_Widget, &o, &p, this);
// };
