/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "colormap.hpp"

#include <cmath>
#include <iostream>
#include <map>
#include <sstream>
#include <string>

#include "elementary.hpp"
#include "kaos_types.hpp"
#include "property.hpp"

using namespace std;
using namespace kaos;
using namespace color;

map<QString, merge_operator> color::merge_opmap(
    {pair<QString, merge_operator>("merge: overwrite", merge_overwrite),
     pair<QString, merge_operator>("merge: greater", merge_greater),
     pair<QString, merge_operator>("merge: add", merge_add),
     pair<QString, merge_operator>("merge: alpha mixer", merge_alpha_mixer)});

QStringList color::merge_oplist() {
  QStringList res;

  for (auto x : merge_opmap) {
    res.push_back(x.first);
  }

  return res;
}

int color::merge_index(color::merge_operator m) {
  int idx = 0;

  for (auto x : merge_opmap) {
    if (x.second == m) {
      return idx;
    }
    idx++;
  }

  cout << "color::merge_index: not found: " << m << endl;
  exit(-1);
}

QColor color::add_colors(QColor a, QColor b) {
  QColor c;
  c.setRedF(fmin(a.redF() + b.redF(), 1));
  c.setGreenF(fmin(a.greenF() + b.greenF(), 1));
  c.setBlueF(fmin(a.blueF() + b.blueF(), 1));
  c.setAlphaF(fmin(a.alphaF() + b.alphaF(), 1));
  return c;
}

QColor greater_of_colors(QColor a, QColor b) {
  QColor c;
  c.setRedF(fmin(fmax(a.redF(), b.redF()), 1));
  c.setGreenF(fmin(fmax(a.greenF(), b.greenF()), 1));
  c.setBlueF(fmin(fmax(a.blueF(), b.blueF()), 1));
  c.setAlphaF(fmin(fmax(a.alphaF(), b.alphaF()), 1));
  return c;
}

QColor alpha_mixer(QColor a, QColor b) {
  float r = b.alphaF();
  QColor c;
  c.setRedF(r * b.redF() + (1 - r) * a.redF());
  c.setGreenF(r * b.greenF() + (1 - r) * a.greenF());
  c.setBlueF(r * b.blueF() + (1 - r) * a.blueF());
  return c;
}

QColor color::merge_colors(QColor a, QColor b, merge_operator op) {
  switch (op) {
    case merge_add:
      return add_colors(a, b);
    case merge_greater:
      return greater_of_colors(a, b);
    case merge_alpha_mixer:
      return alpha_mixer(a, b);
    case merge_overwrite:
      return b;
    default:
      cout << "merge_colors: warning: undefined op: " << op << endl;
      return QColor();
  }
}

// types::color color::qcolor2kcolor(QColor c){
//   return (c.alpha() << 24) | (c.red() << 16) | (c.green() << 8) | (c.blue());
// }

void color::set_widget_background(QWidget *b, QColor c) {
  // thx qt-project.org/forums/viewthread/14377
  QString qss = QString("background-color: %1").arg(c.name());
  b->setStyleSheet(qss);
}

QColor color::scale_color(QColor x, double a) {
  QColor c;
  c.setRedF(x.redF() * a);
  c.setGreenF(x.greenF() * a);
  c.setBlueF(x.blueF() * a);
  c.setAlphaF(x.alphaF() * a);
  return c;
}

color::interpolation::interpolation(int d) {
  if (d < 1) {
    cout << "color::interpolation: invalid dim: " << d << endl;
    exit(-1);
  }

  w = 1;

  value_map = "iterations";

  nodes.resize(2);
  nodes[0].first = 0;
  nodes[0].second = QColor();
  nodes[1].first = 1;
  nodes[1].second = QColor(0xff, 0xff, 0xff);

  apply_dims.clear();
  for (int i = 0; i < d; i++) {
    apply_dims.push_back(false);
  }
  apply_dims[0] = true;
}

color::interpolation::interpolation() : interpolation(1) {}

QColor color::interpolation::compute_color(types::result &r) {
  if (nodes.empty()) {
    return QColor();
  }

  types::value x = property::string_map[value_map](r, apply_dims);

  if (x < nodes[0].first) {
    return nodes[0].second;
  }

  for (int i = 1; i < nodes.size(); i++) {
    if (x < nodes[i].first) {
      double a = (x - nodes[i - 1].first) / (nodes[i].first - nodes[i - 1].first);
      return add_colors(scale_color(nodes[i - 1].second, 1 - a), scale_color(nodes[i].second, a));
    }
  }

  return nodes.back().second;
}

QDataStream &color::operator<<(QDataStream &out, const interpolation &c) {
  return out << c.w << c.value_map << c.apply_dims << c.nodes;
}

QDataStream &color::operator>>(QDataStream &in, interpolation &c) {
  return in >> c.w >> c.value_map >> c.apply_dims >> c.nodes;
}
