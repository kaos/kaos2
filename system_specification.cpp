#include "system_specification.hpp"

#include <algorithm>
#include <iostream>
#include <regex>

using namespace std;
using namespace kaos;

int greatest_param_sym(string s) {
  int highest = -1;
  smatch m;
  regex r("(\\W|^)c\\[[0-9]+\\]");

  while (regex_search(s, m, r, regex_constants::match_any)) {
    for (auto x : m) {
      string s2 = x.str();
      smatch m2;
      regex r2("[0-9]+");

      while (regex_search(s2, m2, r2)) {
        for (auto y : m2) {
          highest = max(highest, stoi(y.str()));
        }
        s2 = m2.suffix().str();
      }
    }
    s = m.suffix().str();
  }

  return highest;
}

int fractal::system_specification::parse_num_params() {
  int highest = -1;

  for (auto s : update_expression) {
    highest = max(highest, greatest_param_sym(s.toStdString()));
  }

  highest = max(highest, greatest_param_sym(condition.toStdString()));

  cout << "parse_num_params: done: " << (highest + 1) << endl;

  return highest + 1;
}

QDataStream &fractal::operator<<(QDataStream &out, const fractal::system_specification &spec) {
  return out << spec.update_expression << spec.condition << spec.maximum_iterations << spec.is_DE << spec.dt;
}

QDataStream &fractal::operator>>(QDataStream &in, fractal::system_specification &spec) {
  return in >> spec.update_expression >> spec.condition >> spec.maximum_iterations >> spec.is_DE >> spec.dt;
}
