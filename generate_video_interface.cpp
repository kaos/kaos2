/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "generate_video_interface.hpp"

#include <iostream>
#include <stdexcept>

#include "fractal_navigator.hpp"

using namespace std;
using namespace kaos;
using namespace kaos::interface;

videox::videox(QWidget *parent, bool get_source_target) : QDialog(parent) {
  fractal_navigator *iface = qobject_cast<fractal_navigator *>(parent);
  QGridLayout *l = new QGridLayout();

  int row = 0;
  l->addWidget(new QLabel("file:"), row, 0);
  l->addWidget(input_filename = new QLineEdit(), row++, 1);
  l->addWidget(new QLabel("width:"), row, 0);
  l->addWidget(input_width = new QLineEdit(), row++, 1);
  l->addWidget(new QLabel("height:"), row, 0);
  l->addWidget(input_height = new QLineEdit(), row++, 1);
  l->addWidget(new QLabel("length:"), row, 0);
  l->addWidget(input_seconds = new QLineEdit(), row++, 1);
  l->addWidget(new QLabel("fps:"), row, 0);
  l->addWidget(input_fps = new QLineEdit(), row++, 1);
  l->addWidget(input_expslow = new QCheckBox("Exp slow"), row++, 1);

  if (get_source_target) {
    l->addWidget(input_f_source = new fractal_lw(this), row, 0);
    l->addWidget(input_f_target = new fractal_lw(this), row++, 1);

    // build fractal list widget
    auto process_list = [this](fractal_lw *lw, fractal_lwi *w) {
      fractal_lwi *item = new fractal_lwi(*w);
      item->block_cleanup = true;  // prevent lwi clones from destroying dl_handle of original list items
      lw->addItem(item);
      lw->setMovement(QListView::Static);
      lw->setMaximumWidth(300);
      lw->setMinimumWidth(200);
      lw->setMinimumHeight(40);
      lw->setSpacing(12);
      lw->setIconSize(QSize(30, 30));
      lw->show();
    };

    int n = iface->contents_widget->count();
    for (int i = 0; i < n; i++) {
      fractal_lwi *w = static_cast<fractal_lwi *>(iface->contents_widget->item(i));
      process_list(input_f_source, w);
      process_list(input_f_target, w);
    }
  }

  QPushButton *b = new QPushButton("EXPORT");
  connect(b, SIGNAL(clicked()), this, SLOT(accept()));
  l->addWidget(b, row, 0);
  setLayout(l);
}

QString videox::get_filename() {
  return input_filename->text();
}

int videox::get_width() {
  return input_width->text().toInt();
}

int videox::get_height() {
  return input_height->text().toInt();
}

int videox::get_seconds() {
  return input_seconds->text().toInt();
}

int videox::get_fps() {
  return input_fps->text().toInt();
}

bool videox::get_expslow() {
  return !!input_expslow->checkState();
}

fractal::specification videox::get_f_source() {
  if (input_f_source->current_item()) {
    return input_f_source->current_item()->spec;
  } else {
    throw runtime_error("No item was selected!");
  }
}

fractal::specification videox::get_f_target() {
  if (input_f_target->current_item()) {
    return input_f_target->current_item()->spec;
  } else {
    throw runtime_error("No item was selected!");
  }
}
