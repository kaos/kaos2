/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once

#include <QtWidgets>
#include <vector>

#include "kaos_types.hpp"

namespace kaos {
namespace fractal {
enum view_type { view_state = 0,
                 view_param,
                 view_num };
typedef QPair<view_type, int> dimtype;

struct view_specification {
  dimtype horizontal;
  dimtype vertical;
  void trim(int ns, int np);  //guarantee view dims fit in ns states and np parameters
  std::pair<std::vector<types::value>, std::vector<types::value> > parameterised_state(QVector<types::value> *base, types::value p, types::value q);
};
QDataStream &operator<<(QDataStream &out, const view_specification &spec);
QDataStream &operator>>(QDataStream &in, view_specification &spec);
};  // namespace fractal
};  // namespace kaos
