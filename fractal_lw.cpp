#include "fractal_lw.hpp"

using namespace std;
using namespace kaos;
using namespace interface;

fractal_lw::fractal_lw(QWidget *p) : QListWidget(p) {
  connect(this, SIGNAL(currentItemChanged(QListWidgetItem *, QListWidgetItem *)), this, SLOT(call_change(QListWidgetItem *, QListWidgetItem *)));
}

fractal_lwi *fractal_lw::current_item() {
  return (fractal_lwi *)currentItem();
}

void fractal_lw::call_change(QListWidgetItem *c, QListWidgetItem *p) {
  emit change((fractal_lwi *)c, (fractal_lwi *)p);
}

fractal::specification *fractal_lw::get_spec() {
  fractal_lwi *lwi = current_item();
  if (lwi)
    return &lwi->spec;
  else
    return 0;
}
