#include "view_specification.hpp"

#include "kaos_types.hpp"

using namespace kaos;
using namespace std;

void fractal::view_specification::trim(int ns, int np) {
  // check horizontal
  if (horizontal.first == fractal::view_state && horizontal.second >= ns) {
    horizontal.second = ns - 1;
  } else if (horizontal.first == fractal::view_param && horizontal.second >= np) {
    horizontal.second = np - 1;
  }

  // check vertical
  if (vertical.first == fractal::view_state && vertical.second >= ns) {
    vertical.second = ns - 1;
  } else if (vertical.first == fractal::view_param && vertical.second >= np) {
    vertical.second = np - 1;
  }

  // todo: check inequality?
}

pair<vector<types::value>, vector<types::value> > fractal::view_specification::parameterised_state(QVector<types::value> *base, types::value p, types::value q) {
  QVector<types::value> a[2] = {base[0], base[1]};

  a[horizontal.first][horizontal.second] = p;
  a[vertical.first][vertical.second] = q;

  return make_pair(a[0].toStdVector(), a[1].toStdVector());
}

// todo: write me
QDataStream &fractal::operator<<(QDataStream &out, const fractal::view_specification &spec) {
  QPair<int, int> h(spec.horizontal.first, spec.horizontal.second), v(spec.vertical.first, spec.vertical.second);
  return out << h << v;
}

QDataStream &fractal::operator>>(QDataStream &in, fractal::view_specification &spec) {
  QPair<int, int> h, v;
  in >> h >> v;
  spec.horizontal.first = (fractal::view_type)h.first;
  spec.horizontal.second = h.second;
  spec.vertical.first = (fractal::view_type)v.first;
  spec.vertical.second = v.second;
  return in;
}
