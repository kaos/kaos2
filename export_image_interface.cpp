/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "export_image_interface.hpp"

using namespace kaos::interface;

imagex::imagex(QWidget *parent) : QDialog(parent) {
  QGridLayout *l = new QGridLayout();

  l->addWidget(new QLabel("file:"), 0, 0);
  l->addWidget(input_filename = new QLineEdit(), 0, 1);
  l->addWidget(new QLabel("width:"), 1, 0);
  l->addWidget(input_width = new QLineEdit(), 1, 1);
  l->addWidget(new QLabel("height:"), 2, 0);
  l->addWidget(input_height = new QLineEdit(), 2, 1);

  QPushButton *b = new QPushButton("EXPORT");
  connect(b, SIGNAL(clicked()), this, SLOT(accept()));
  l->addWidget(b, 3, 0);
  setLayout(l);
}

QString imagex::get_filename() {
  return input_filename->text();
}

int imagex::get_width() {
  return input_width->text().toInt();
}

int imagex::get_height() {
  return input_height->text().toInt();
}
