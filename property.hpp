/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once

#include <QtWidgets>
#include <functional>

#include "kaos_types.hpp"

namespace kaos {
namespace property {
/*! function type for condensing a kaos::types::result to a
        double */
// typedef double (*f_condense)(types::result &r, QList<bool> apply_dims);
typedef std::function<double(const types::result &r, QList<bool> apply_dims)> f_condense;

/* /\*! names of properties *\/ */
/* const QStringList property_name({"iterations", "perm. entropy", "end value", "rotation"}); */

/* /\*! enum listing the different properties *\/ */
/* enum property_id{mtype_iterations, mtype_pentropy, mtype_endvalue, mtype_rotation}; */

/* /\*! Available property functions *\/ */
/* const QList<f_condense> property_function({iterations, pentropy, endvalue, rotation}); */

/*! iterations property map
      @param res point iteration data 
    */
double iterations(const types::result &res, QList<bool> apply_dims);

/*! permutation entropy ord 2 property map
      @param res point iteration data 
    */
double pentropy(const types::result &res, QList<bool> apply_dims);

double lyapunov_exponent(const types::result &res, QList<bool> apply_dims);

/*! permutation entropy ord 4 property map
      @param res point iteration data 
    */
double pentropy4(const types::result &res, QList<bool> apply_dims);

/*! end value property map
      @param res point iteration data 
    */
double endvalue(const types::result &res, QList<bool> apply_dims);

/*! rotation property map
      @param res point iteration data 
    */
double rotation(const types::result &res, QList<bool> apply_dims);

extern std::map<QString, f_condense> string_map;

QStringList string_list();

/*! subroutine used by \ref pentropy. Returns a string with the
        value sorting ranks of n values in x seperated by d index
        steps. 
	@param x data buffer
	@param d data point seperation
	@param n number of data points
    */
std::string rank_string(std::vector<types::it_point> x, int n);

};  // namespace property
};  // namespace kaos
