#include <cmath>
#include <functional>
#include <vector>

#include "fractal_specification_utility.hpp"
#include "kaos_types.hpp"
#include "utility.hpp"

using namespace std;
using namespace kaos;
using namespace types;

extern "C" kaos::types::result compute(vector<double> x, vector<double> c, int max_its) {
  unsigned int count = 0;
  result res;
  int d = x.size();
  double h = 1e-4 * (l2norm(x) + l2norm(c));
  vector<value> y = x;
  vector<value> x2 = x + h * vector<value>(d, 1);
  vector<value> y2 = x2;

  res.dimension = d;
  res.max_iterations = max_its;
  res.data.resize(d);
  for (int i = 0; i < d; i++) res.data[i].resize(max_its);

  function<vector<double>(vector<double>)> f = [c](vector<double> x) {
    vector<double> res(@dim);
    @fupd;
    return res;
  };

  // main loop
  double dt = @dt;
  for (count = 0; count < max_its && (@condition); count++) {
    for (int i = 0; i < d; i++) {
      res.data[i][count].x = y[i];
      res.data[i][count].x2 = y2[i];
    }
    @expr_upd1;
    @expr_upd2;
    x = y;
    x2 = y2;
  }

  res.iterations = count;
  return res;
}
