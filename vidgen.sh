#!/bin/bash
[ -f export/$1 ] && rm export/$1
ffmpeg -framerate $2 -i vidbuf/frame_%d.png -vcodec libx264 -crf 18 export/$1
rm vidbuf/*
