/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fractal_templates.hpp"

using namespace std;
using namespace kaos;

map<QString, fractal::specification> fractal::templates;

QStringList fractal::template_list() {
  QStringList v;

  for (auto x : templates) {
    v.push_back(x.first);
  }

  return v;
}

void fractal::build_templates() {
  fractal::specification s;

  // basic configuration
  s.system.condition = "pow(x[0],2) + pow(x[1],2) < 4";
  s.system.maximum_iterations = 50;
  s.system.is_DE = false;
  s.system.dt = 0.1;
  s.view.horizontal = dimtype(view_param, 0);
  s.view.vertical = dimtype(view_param, 1);
  s.bounds.bounds[0] = -2;
  s.bounds.bounds[1] = 2;
  s.bounds.bounds[2] = -2;
  s.bounds.bounds[3] = 2;
  s.bounds.base[view_state] = QVector<double>({0, 0});
  s.bounds.base[view_param] = QVector<double>({0, 0});
  s.color.interpolators.resize(1);
  s.color.interpolators[0].first.value_map = "iterations";
  s.color.interpolators[0].first.nodes.resize(2);
  s.color.interpolators[0].first.nodes[0].first = 0;
  s.color.interpolators[0].first.nodes[0].second = QColor(0, 0, 0);
  s.color.interpolators[0].first.nodes[1].first = 1;
  s.color.interpolators[0].first.nodes[1].second = QColor(255, 255, 255);
  s.color.interpolators[0].first.apply_dims = QList<bool>({true, false});

  // 2d maps

  // mandelbrot
  s.system.update_expression = QVector<QString>({"pow(x[0],2) - pow(x[1],2) + c[0]", "2 * x[0] * x[1] + c[1]"});
  s.label = "Mandelbrot";

  fractal::templates["Mandelbrot"] = s;

  // newton
  s.view.horizontal = dimtype(view_state, 0);
  s.view.vertical = dimtype(view_state, 1);
  s.bounds.base[view_param] = QVector<double>({0.5, 0.5});
  s.system.update_expression = QVector<QString>({"pow(x[0], 3) - c[0] * pow(x[0], 2) - 3 * x[0] * pow(x[1], 2) + 2 * x[0] * x[1] * c[1] - x[0] + c[0] * pow(x[1], 2) + c[0]", "3 * pow(x[0], 2) * x[1] - pow(x[0], 2) * c[1] - 2 * x[0] * x[1] * c[0] - pow(x[1], 3) + c[1] * pow(x[1], 2) - x[1] + c[1]"});
  s.label = "Newton";

  fractal::templates["Newton"] = s;

  // coupled logistics
  s.view.horizontal = dimtype(view_param, 0);
  s.view.vertical = dimtype(view_param, 3);
  s.bounds.base[view_param] = QVector<double>({1, 0.2, 1, 1, 0.2});
  s.bounds.base[view_state] = QVector<double>({10, 1});
  s.bounds.bounds[0] = 1.85;
  s.bounds.bounds[1] = 4.5;
  s.bounds.bounds[2] = 0;
  s.bounds.bounds[3] = 0.5;
  s.color.interpolators.resize(2);
  s.color.interpolators[0].first.value_map = "perm. entropy2";
  s.color.interpolators[0].first.nodes.resize(2);
  s.color.interpolators[0].first.nodes[0].first = 0;
  s.color.interpolators[0].first.nodes[0].second = QColor(0, 0, 0);
  s.color.interpolators[0].first.nodes[1].first = 1;
  s.color.interpolators[0].first.nodes[1].second = QColor(255, 0, 0);
  s.color.interpolators[0].first.apply_dims = QList<bool>({true, false});
  s.color.interpolators[1].first.value_map = "perm. entropy2";
  s.color.interpolators[1].first.nodes.resize(2);
  s.color.interpolators[1].first.nodes[0].first = 0;
  s.color.interpolators[1].first.nodes[0].second = QColor(0, 0, 0);
  s.color.interpolators[1].first.nodes[1].first = 1;
  s.color.interpolators[1].first.nodes[1].second = QColor(0, 0, 255);
  s.color.interpolators[1].first.apply_dims = QList<bool>({false, true});
  s.color.interpolators[1].second = color::merge_add;

  s.system.update_expression = QVector<QString>({"fmax(x[0] * (c[0] - c[1] * x[0]) - c[2] * x[1], 0)", "fmax(x[1] * (c[3] * x[0] - c[4] * x[1]), 0)"});
  s.system.condition = "x[0] + x[1] > 0";
  s.label = "Coupled logistics";

  fractal::templates["Coupled logistics"] = s;

  // duffing map
  s.view.horizontal = dimtype(view_param, 0);
  s.view.vertical = dimtype(view_param, 1);
  s.bounds.base[view_param] = QVector<double>({-0.2, 2.75});
  s.bounds.base[view_state] = QVector<double>({1, 1});
  s.bounds.bounds[0] = -2;
  s.bounds.bounds[1] = 2;
  s.bounds.bounds[2] = -1;
  s.bounds.bounds[3] = 5;
  s.color.interpolators[0].first.value_map = "perm. entropy2";
  s.color.interpolators[1].first.value_map = "perm. entropy2";
  s.system.update_expression = QVector<QString>({"x[1]", "c[0] * x[0] + c[1] * x[1] - pow(x[1], 3)"});
  s.system.condition = "pow(x[0],2) + pow(x[1],2) < 4";
  s.label = "Duffing map";

  fractal::templates["Duffing map"] = s;

  // Henon map
  s.view.horizontal = dimtype(view_param, 0);
  s.view.vertical = dimtype(view_param, 1);
  s.bounds.base[view_param] = QVector<double>({0, 0});
  s.bounds.base[view_state] = QVector<double>({0, 0});
  s.bounds.bounds[0] = -1;
  s.bounds.bounds[1] = 3;
  s.bounds.bounds[2] = -2;
  s.bounds.bounds[3] = 2;
  s.system.update_expression = QVector<QString>({"1 - c[0] * pow(x[0], 2) + x[1]", " c[1] * x[0]"});
  s.label = "Henon map";

  fractal::templates["Henon map"] = s;

  // 1d maps
  // gauss map
  s.view.vertical = dimtype(view_param, 1);
  s.view.horizontal = dimtype(view_state, 0);
  s.bounds.bounds[0] = -1.4;
  s.bounds.bounds[1] = 1.4;
  s.bounds.bounds[2] = -1;
  s.bounds.bounds[3] = 0.35;
  s.bounds.base[view_param] = QVector<double>({6.2, 1});
  s.bounds.base[view_state] = QVector<double>({0});
  s.color.interpolators.resize(1);
  s.color.interpolators[0].first.value_map = "Lyapunov exp.";
  s.system.update_expression = QVector<QString>({"exp(-c[0] * pow(x[0], 2)) + c[1]"});
  s.color.interpolators[0].first.apply_dims = QList<bool>({true});
  s.label = "Gauss map";

  fractal::templates["Gauss map"] = s;

  // arnold
  s.view.horizontal = dimtype(view_param, 0);
  s.view.vertical = dimtype(view_param, 1);
  s.system.update_expression = QVector<QString>({"fmod(x[0] + c[0] - c[1] / (2 * M_PI) * sin(2 * M_PI * x[0]), 1)"});
  s.system.condition = "true";
  s.system.maximum_iterations = 10;
  s.bounds.base[view_param] = QVector<double>({0, 0});
  s.bounds.base[view_state] = QVector<double>({0});
  s.color.interpolators[0].first.value_map = "rotation";
  s.color.interpolators[0].first.nodes.resize(3);
  s.color.interpolators[0].first.nodes[0].first = -1;
  s.color.interpolators[0].first.nodes[0].second = QColor(0, 0, 255);
  s.color.interpolators[0].first.nodes[1].first = 0;
  s.color.interpolators[0].first.nodes[1].second = QColor(0, 0, 0);
  s.color.interpolators[0].first.nodes[2].first = 1;
  s.color.interpolators[0].first.nodes[2].second = QColor(255, 0, 0);
  s.bounds.bounds[0] = -2;
  s.bounds.bounds[1] = 2;
  s.bounds.bounds[2] = -12;
  s.bounds.bounds[3] = 12;
  s.label = "Arnold";

  fractal::templates["Arnold"] = s;

  // ODE maps

  // Lorenz attractor
  s.view.horizontal = dimtype(view_param, 0);
  s.view.vertical = dimtype(view_param, 1);
  s.system.update_expression = QVector<QString>({"c[0] * (x[1] - x[0])",
                                                 "x[0] * (c[1] - x[2]) - x[1]",
                                                 "x[0] * x[1] - c[2] * x[2]"});
  s.system.condition = "true";
  s.system.maximum_iterations = 50;
  s.system.is_DE = true;
  s.bounds.base[view_param] = QVector<double>({10, 28, 8 / (double)3});
  s.bounds.base[view_state] = QVector<double>({1, 1, 1});
  s.color.interpolators.resize(2);
  s.color.interpolators[0].first.value_map = "perm. entropy2";
  s.color.interpolators[0].first.nodes.resize(2);
  s.color.interpolators[0].first.nodes[0].first = 0;
  s.color.interpolators[0].first.nodes[0].second = QColor(0, 0, 0);
  s.color.interpolators[0].first.nodes[1].first = 1;
  s.color.interpolators[0].first.nodes[1].second = QColor(0, 0, 255);
  s.color.interpolators[0].first.apply_dims = QList<bool>({true, false, false});
  s.color.interpolators[1].first.value_map = "perm. entropy2";
  s.color.interpolators[1].first.nodes.resize(2);
  s.color.interpolators[1].first.nodes[0].first = 0;
  s.color.interpolators[1].first.nodes[0].second = QColor(0, 0, 0);
  s.color.interpolators[1].first.nodes[1].first = 1;
  s.color.interpolators[1].first.nodes[1].second = QColor(0, 255, 0);
  s.color.interpolators[1].first.apply_dims = QList<bool>({false, true, false});
  s.bounds.bounds[0] = -30;
  s.bounds.bounds[1] = 60;
  s.bounds.bounds[2] = -120;
  s.bounds.bounds[3] = 160;
  s.label = "Lorenz";

  fractal::templates["Lorenz"] = s;

  // A New Simple Chaotic System with One Nonlinear Term, Bouteraa et al.
  s.view.horizontal = dimtype(view_param, 2);
  s.view.vertical = dimtype(view_param, 3);
  s.system.update_expression = QVector<QString>({
    "c[0] + c[1] * x[1] + c[2] * x[0] * (0.0 + 0.01 * x[3])",
    "c[3] * x[0] + c[4] * x[1] + c[5] * x[2] + c[6] * x[3]",
    "c[7] * x[1]",
    "c[8] * x[0] + c[9] * x[2]"
  });

  // a1=7,a2=1.3,a3=−9,a4=7.8,a5=−1.5,a6=−8.5,a7=2.7,a8=7.8,a9=11.6,a10=−5.7.
  s.bounds.base[view_param] = QVector<double>({7, 1.3, -9, 7.8, -1.5, -8.5, 2.7, 7.8, 11.6, -5.7});
  s.bounds.base[view_state] = QVector<double>({-0.4, -0.03, -0.004, -0.001}); 
  s.bounds.bounds[0] = -70;
  s.bounds.bounds[1] = 0;
  s.bounds.bounds[2] = -490;
  s.bounds.bounds[3] = -390;
  s.system.condition = "true";
  s.system.maximum_iterations = 200;
  s.system.is_DE = true;
  s.color.interpolators.resize(1);
  s.color.interpolators[0].first.value_map = "perm. entropy2";
  s.color.interpolators[0].first.apply_dims = QList<bool>({true, false, false, false});
  s.label = "Bouteraa et al.";

  fractal::templates["Bouteraa et al."] = s;

}
