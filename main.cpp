/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <locale.h>

#include <QApplication>
#include <QThreadPool>

#include "fractal_mainwindow.hpp"
#include "fractal_navigator.hpp"
#include "fractal_templates.hpp"

int main(int argc, char **argv) {
  // start qt
  QApplication app(argc, argv);

  // read stylesheet
  QFile File("stylesheet.qss");
  File.open(QFile::ReadOnly);
  QString StyleSheet = QString::fromUtf8(File.readAll().data());
  app.setStyleSheet(StyleSheet);

  // to get invertible string/number parsing
  setlocale(LC_NUMERIC, "C");

  // build templates
  kaos::fractal::build_templates();

  // use for sequential debugging
  // QThreadPool::globalInstance() -> setMaxThreadCount(1);

  // build main window
  kaos::interface::fractal_mainwindow mw;

  // wait for terminate
  return app.exec();
}
