/* The MIT License (MIT)

   Copyright (c) 2014 Ross Linscott

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
   OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
   WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once

#include <QtWidgets>

#include "fractal_image.hpp"
#include "fractal_job.hpp"
#include "fractal_lw.hpp"
#include "fractal_lwi.hpp"
#include "fractal_specification.hpp"
#include "generate_video_interface.hpp"

namespace kaos {
namespace interface {

/*! \brief Main interface class

      The fractal navigator handles creating, editing and swapping
      between different fractals as well as visualisation, navigation
      and parameters.
    */
class fractal_navigator : public QWidget {
  Q_OBJECT

 private:
  // background computation stuff
  std::vector<fractal::job> call_points;
  QFutureWatcher<QRgb> fwatch;

  // configurations
  QSize resolution;

  // data tracking
  /*! track worker fractal widget */
  fractal_lwi *worker_fractal;

  /*! widget handling visualisation */
  fractal::image *image_label;

  // private functions
  void update_jobs(fractal::specification *s);
  void update_bounds(double xmin, double xmax, double ymin, double ymax);
  void translate(double x, double y);
  void static_zoom(double a);
  void start_compute();
  void template_selected(QString s);
  fractal::specification *set_worker_spec();
  bool generate_image(QString fname, fractal::specification spec, bool prog);

 public:
  /*! widget for selecting a fractal */
  fractal_lw *contents_widget;

  /*! \brief default contsructor */
  fractal_navigator(QWidget *parent = 0);

 public slots:
  void edit_system();
  void edit_bounds();
  void edit_view();
  void edit_color();
  void edit_resolution();
  void resolution_to_image_size();
  void reload_data();
  void unload_data();
  void zoom(QRect r);
  void update_fractal();
  void zoom_in();
  void zoom_out();
  void translate_up();
  void translate_down();
  void translate_left();
  void translate_right();
  void julia_from_mouse();
  void change_page(fractal_lwi *current, fractal_lwi *previous);
  void add_fractal(fractal::specification spec);
  void template_selected(QAction *a);
  void generate_video_path(std::vector<std::vector<double>> path);

  void copy_current();
  void export_current();
  void generate_video_ab();
  void generate_video(interface::videox *v, std::function<fractal::specification(double r)> fgen);
  void save_current();
  void load_file();
  void set_record_path();
  void delete_current();
  void new_fractal();
};
};  // namespace interface

namespace fractal {
QRgb draw_pixel(job j);
};

};  // namespace kaos
