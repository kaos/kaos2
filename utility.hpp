#include <string>
#include <vector>

#include "kaos_types.hpp"

using namespace std;

namespace kaos {
std::string get_file(std::string filename);

template <typename T>
T l2norm(std::vector<T> x) {
  T sum = 0;
  for (auto y : x) sum += pow(y, 2);
  return sqrt(sum);
}

template <typename T>
std::vector<T> operator-(const std::vector<T> &a, const std::vector<T> &b) {
  std::vector<T> res = a;
  for (int i = 0; i < res.size(); i++) res[i] -= b[i];
  return res;
}

template <typename T>
std::vector<T> operator+(const std::vector<T> &a, const std::vector<T> &b) {
  std::vector<T> res = a;
  for (int i = 0; i < res.size(); i++) res[i] += b[i];
  return res;
}

template <typename T>
std::vector<T> operator*(const double a, const std::vector<T> &b) {
  std::vector<T> res = b;
  for (int i = 0; i < res.size(); i++) res[i] *= a;
  return res;
}
};  // namespace kaos