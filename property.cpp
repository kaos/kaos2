/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "property.hpp"

#include <cmath>
#include <iostream>
#include <map>
#include <sstream>
#include <string>

#include "kaos_types.hpp"
#include "utility.hpp"

using namespace std;
using namespace kaos;
using namespace property;

map<QString, f_condense> property::string_map({pair<QString, f_condense>("iterations", iterations),
                                               pair<QString, f_condense>("Lyapunov exp.", lyapunov_exponent),
                                               pair<QString, f_condense>("perm. entropy2", pentropy),
                                               pair<QString, f_condense>("perm. entropy4", pentropy4),
                                               pair<QString, f_condense>("end value", endvalue),
                                               pair<QString, f_condense>("rotation", rotation)});

double sigmoid(double x) {
  return (atan(x) + (M_PI / 2)) / M_PI;
}

double pm_sigmoid(double x) {
  return atan(x) * 2 / M_PI;
}

double property::iterations(const types::result &res, QList<bool> apply_dims) {
  return res.iterations / (double)res.max_iterations;
}

double property::endvalue(const types::result &res, QList<bool> apply_dims) {
  int d = res.dimension;
  int n = res.iterations;
  int sum = 0;

  for (int i = 0; i < d; i++) sum += apply_dims[i] * res.data[i][n - 1].x;
  return sum;
}

double property::pentropy(const types::result &res, QList<bool> apply_dims) {
  int count = 0;
  int sum = 0;
  int num = 0;
  int d = res.dimension;
  int n = res.iterations;
  int i, j;

  for (i = 0; i < d; i++) {
    if (apply_dims[i]) {
      count = 0;
      for (j = 0; j < n - 2; j++) {
        count += res.data[i][j].x < res.data[i][j + 1].x;
      }
      sum += count;
      num++;
    }
  }

  return sum / (double)(num * n);
}

double property::lyapunov_exponent(const types::result &res, QList<bool> apply_dims) {
  auto get_delta = [&res](int j) -> double {
    vector<types::value> delta = res.get_row2(j) - res.get_row(j);
    return l2norm(delta);
  };

  double h0 = get_delta(0);

  if (h0 == 0) {
    return 0;
  }

  double lambda = 0;
  for (int j = 1; j < res.iterations; j++) {
    double l = 1 / (double)j * log(get_delta(j) / h0);
    lambda = fmax(l, lambda);
  }

  return lambda;
}

double pentropy_n(int n, const types::result &res, QList<bool> apply_dims) {
  int i, j;
  int d = res.dimension;
  int num = res.iterations;
  double sum = 0;
  map<string, int> count;

  for (i = 0; i < d; i++) {
    if (apply_dims[i]) {
      count.clear();
      for (j = 0; j < num - 1 - n; j++) {
        vector<types::it_point> buf(res.data[i].begin() + j, res.data[i].begin() + j + n);
        string v = rank_string(buf, n);
        count[v] = count[v] + 1;
      }

      for (auto x : count) {
        // add entropy components to sum ..
        sum += log(x.second / (double)num);
      }
    }
  }

  if (sum > 0) {
    cout << "warning: positive entropy sum: " << sum << endl;
  }

  return exp(sum / n);
}

double property::pentropy4(const types::result &res, QList<bool> apply_dims) {
  return pentropy_n(4, res, apply_dims);
}

float k_modulus(double x, double p) {
  int num = floor(x / p);
  return x - num * p;
}
float point_angle(double x, double y) {
  return atan2(x, y);
}
float angle_difference(double a, double b) {
  return k_modulus(a - b + M_PI, 2 * M_PI) - M_PI;
}

double property::rotation(const types::result &res, QList<bool> apply_dims) {
  int i, j;
  int d = res.dimension;
  int n = res.iterations;
  double sum = 0;

  int ndims = 0;
  for (auto x : apply_dims) ndims += x;
  vector<double> buf(ndims), buf2(ndims);

  if (ndims == 2) {
    for (j = 0; j < n; j++) {
      int k = 0;
      for (i = 0; i < d; i++) {
        if (apply_dims[i]) {
          buf[k++] = res.data[i][j].x;
        }
      }
      if (j > 0) {
        sum += angle_difference(point_angle(buf[0], buf[1]), point_angle(buf2[0], buf2[1]));
      }
    }
    buf2 = buf;
  } else {
    for (j = 0; j < n; j++) {
      for (i = 0; i < d; i++) {
        if (apply_dims[i]) sum += res.data[i][j].x;
      }
    }
  }

  return pm_sigmoid(sum / 10);
}

string property::rank_string(vector<types::it_point> x, int n) {
  int i, j;
  ostringstream v;
  vector<int> ranks(n);

  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      ranks[i] += x[i].x < x[j].x;
    }
    v << ranks[i];
  }

  return v.str();
}

QStringList property::string_list() {
  QStringList res;

  for (auto x : string_map) {
    res.push_back(x.first);
  }

  return res;
}
