/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once

#include <QtWidgets>

#include "kaos_types.hpp"
#include "view_specification.hpp"

namespace kaos {
namespace fractal {
// bounds and base values
struct bounds_specification {
  /*! horizonalt lower/upper, then vertical lower/upper view value bounds */
  QVector<types::value> bounds;

  /*! base values of parameters; non-view parameters use base values */
  QVector<types::value> base[view_num];

  bounds_specification();
};
QDataStream &operator<<(QDataStream &out, const bounds_specification &spec);
QDataStream &operator>>(QDataStream &in, bounds_specification &spec);
};  // namespace fractal
};  // namespace kaos
