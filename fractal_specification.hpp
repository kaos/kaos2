/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once

#include <QtWidgets>
#include <string>
#include <vector>

#include "bounds_specification.hpp"
#include "color_specification.hpp"
#include "kaos_types.hpp"
#include "system_specification.hpp"
#include "view_specification.hpp"

namespace kaos {
namespace fractal {

/*! \brief Configuration for a fractal visualisation */
class specification {
  static int libidx;

 public:
  system_specification system;
  view_specification view;
  bounds_specification bounds;
  color_specification color;
  QString label;

  void *dl_handle;
  std::string libname;
  bool require_compile;

  /*! point iteration data computation function */
  types::compute_point f_compute_point;

  static specification between(specification a, specification b, double r);
  static specification weighted(std::vector<double> w, std::vector<specification> x);
  specification();
  void cleanup();

  bool compile();

  /*! Point iteration call routine
	@param p,q values for horizontal and vertical view dimension
      */
  types::result compute_point(types::value p, types::value q);

  std::vector<dimtype> nonview_dims();
  void set_dimension_value(dimtype d, types::value x);
  int dimension();

  std::string summary();
};
QDataStream &operator<<(QDataStream &out, const kaos::fractal::specification &spec);
QDataStream &operator>>(QDataStream &in, kaos::fractal::specification &spec);
};  // namespace fractal
};  // namespace kaos
