/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "fractal_specification.hpp"

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <QString>
#include <boost/format.hpp>
#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <streambuf>

#include "elementary.hpp"
#include "kaos_types.hpp"
#include "utility.hpp"

using namespace kaos;
using namespace std;

int fractal::specification::libidx = 0;

// todo: look up how to free a dlsym after dlclose

fractal::specification::specification() {
  dl_handle = 0;
  require_compile = true;
}

fractal::specification fractal::specification::weighted(vector<double> w, vector<fractal::specification> x) {
  if (x.empty() || w.size() != x.size()) {
    throw runtime_error("Invalid spec weighting parameters!");
  }

  double wsum = 0;
  for (auto y : w) wsum += y;

  if (!(wsum > 0)) {
    throw runtime_error("Invalid weights!");
  }

  specification res = x.front();

  // transform bounds
  for (int i = 0; i < 4; i++) {
    res.bounds.bounds[i] = 0;
    for (int j = 0; j < x.size(); j++) res.bounds.bounds[i] += w[j] * x[j].bounds.bounds[i];
    res.bounds.bounds[i] /= wsum;
  }

  // transform iterations
  res.system.maximum_iterations = 0;
  for (int j = 0; j < x.size(); j++) res.system.maximum_iterations += w[j] * x[j].system.maximum_iterations;
  res.system.maximum_iterations /= wsum;

  // transform parameters
  for (int k = 0; k < fractal::view_num; k++) {
    for (int i = 0; i < res.bounds.base[k].size(); i++) {
      res.bounds.base[k][i] = 0;
      for (int j = 0; j < x.size(); j++) res.bounds.base[k][i] += w[j] * x[j].bounds.base[k][i];
      res.bounds.base[k][i] /= wsum;
    }
  }

  return res;
}

fractal::specification fractal::specification::between(fractal::specification a, fractal::specification b, double r) {
  return weighted({(1 - r), r}, {a, b});
}

void fractal::specification::cleanup() {
  if (dl_handle) {
    dlclose(dl_handle);
    std::system(("rm " + libname).c_str());
    dl_handle = 0;
    require_compile = true;
  }
}

types::result fractal::specification::compute_point(types::value p, types::value q) {
  pair<vector<types::value>, vector<types::value> > pstate = view.parameterised_state(&bounds.base[0], p, q);
  return f_compute_point(pstate.first, pstate.second, system.maximum_iterations);
}

bool fractal::specification::compile() {
  cleanup();
  libname = "kaos_tmplib_" + to_string(libidx++) + ".so";
  int i;
  int d = system.update_expression.size();

  string source_str = get_file("compute_template.cpp");

  source_str = regex_replace(source_str, regex("@dim"), to_string(d));
  source_str = regex_replace(source_str, regex("@dt"), to_string(system.dt));
  source_str = regex_replace(source_str, regex("@condition"), system.condition.toStdString());

  ostringstream source(string(""));
  for (i = 0; i < d; i++) {
    source << "    res[" << i << "] = " << system.update_expression[i].toStdString() << ";" << endl;
  }
  source_str = regex_replace(source_str, regex("@fupd"), source.str());
  source.clear();

  if (system.is_DE) {
    source_str = regex_replace(source_str, regex("@expr_upd1"), "y = y + rk4_scheme(f, x, dt)");
    source_str = regex_replace(source_str, regex("@expr_upd2"), "y2 = y2 + rk4_scheme(f, x2, dt)");
  } else {
    source_str = regex_replace(source_str, regex("@expr_upd1"), "y = f(x)");
    source_str = regex_replace(source_str, regex("@expr_upd2"), "y2 = f(x2)");
  }

  // build shared object
  string cc = "g++ -O3 -xc++ -fPIC -shared -o " + libname + " -";

  cout << "compiling: " << endl
       << source_str << endl;
  FILE *p = popen(cc.c_str(), "w");
  fwrite(source_str.c_str(), 1, source_str.length(), p);

  if (pclose(p) < 0) {
    cout << "COMPILATION FAILED!" << endl;
    return false;
  }

  // load according to
  // stackoverflow.com/questions/1142103/how-do-i-load-a-shared-object-in-c
  char *error;
  string dlfile = "./" + libname;
  dl_handle = dlopen(dlfile.c_str(), RTLD_NOW);
  if (!dl_handle) {
    cout << "dlopen failed!" << endl;
    fprintf(stderr, "%s\n", dlerror());
    exit(-1);
  }
  dlerror(); /* Clear any existing error */
  f_compute_point = (types::compute_point)dlsym(dl_handle, "compute");
  if ((!f_compute_point) || (error = dlerror()) != NULL) {
    cout << "dlsym failed!" << endl;
    fprintf(stderr, "%s\n", error);
    exit(-1);
  }

  cout << "compilation complete!" << endl;

  require_compile = false;

  // set compute_point for spec
  return f_compute_point;
}

vector<fractal::dimtype> fractal::specification::nonview_dims() {
  vector<fractal::dimtype> res;
  int i;
  dimtype d;

  for (i = 0; i < bounds.base[fractal::view_state].size(); i++) {
    d = dimtype(view_state, i);
    if (d != view.horizontal && d != view.vertical) {
      res.push_back(d);
    }
  }

  for (i = 0; i < bounds.base[fractal::view_param].size(); i++) {
    d = dimtype(view_param, i);
    if (d != view.horizontal && d != view.vertical) {
      res.push_back(d);
    }
  }

  return res;
}

void fractal::specification::set_dimension_value(dimtype d, types::value x) {
  bounds.base[d.first][d.second] = x;
}

int fractal::specification::dimension() {
  return system.update_expression.size();
}

QDataStream &fractal::operator<<(QDataStream &out, const specification &spec) {
  return out << spec.system << spec.view << spec.bounds << spec.color << spec.label;
}

QDataStream &fractal::operator>>(QDataStream &in, specification &spec) {
  spec.cleanup();
  spec.dl_handle = 0;
  spec.require_compile = true;
  return in >> spec.system >> spec.view >> spec.bounds >> spec.color >> spec.label;
}
