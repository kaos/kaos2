/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include <QtWidgets>
#include <string>
#include <vector>

#include "kaos_types.hpp"
#include "property.hpp"

namespace kaos {
namespace color {

struct interpolation {
  double w;
  QString value_map;
  QList<bool> apply_dims;
  QVector<QPair<types::value, QColor> > nodes;

  interpolation(int dim);
  interpolation();
  QColor compute_color(types::result &r);
};

QDataStream &operator<<(QDataStream &out, const interpolation &c);
QDataStream &operator>>(QDataStream &in, interpolation &c);

/*! scale a QColor object by a scalar
      @param x color to scale
      @param a scale factor
    */
QColor scale_color(QColor x, double a);

enum merge_operator { merge_overwrite,
                      merge_greater,
                      merge_alpha_mixer,
                      merge_add };

extern std::map<QString, merge_operator> merge_opmap;

QStringList merge_oplist();
int merge_index(merge_operator m);

QColor add_colors(QColor a, QColor b);

/*! Merge colors to a single color by summation
      @param c std::vector of colors to merge
    */
QColor merge_colors(QColor a, QColor b, merge_operator c);

/*! convert a QColor to an int
      @param c color to convert
    */
/* types::color qcolor2kcolor(QColor c); */

/*! set the background color of a QWidget
      @param b widget to set background
      @param c color to set
    */
void set_widget_background(QWidget *b, QColor c);
};  // namespace color
};  // namespace kaos
