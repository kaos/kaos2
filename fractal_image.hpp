/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include <QtWidgets>
#include <vector>

namespace kaos {
namespace fractal {
/*! \brief Extended QLabel which displays an image and supports a
        selection rectangle */
class image : public QLabel {
  Q_OBJECT

 private:
  QImage *buf;
  std::vector<std::vector<double>> animation_path;

 public:
  bool record_path;

  /*! \brief Horizontal min value */
  double xmin;
  /*! \brief Horizontal max value */
  double xmax;
  /*! \brief Vertical min value */
  double ymin;
  /*! \brief Vertical max value */
  double ymax;

  /*! Constructor
	@param parent QWidget passed to QLabel
      */
  image(QWidget *parent = 0);
  void update_bounds(double *b);
  void set_image(QImage *b);
  std::vector<double> mouse2coords();

 protected:
  /*! Paints the QLabel and adds the selection rectangle */
  void paintEvent(QPaintEvent *e);

  /*! Registers the UL selection rectangle corner */
  void mousePressEvent(QMouseEvent *e);

  /*! Updates the selection rectangle */
  void mouseMoveEvent(QMouseEvent *e);

  /*! Triggers selected() */
  void mouseReleaseEvent(QMouseEvent *e);

 private:
  bool selection_started;
  QRect selection_rect;

 public:
 signals:
  /*! The QRect r has been selected on this image */
  void selected(QRect r);
  void path_created(std::vector<std::vector<double>> p);
};
};  // namespace fractal
};  // namespace kaos
