#include "utility.hpp"

#include <fstream>
#include <string>

using namespace std;
using namespace kaos;

string kaos::get_file(string filename) {
  std::ifstream ifs(filename);
  std::string content((std::istreambuf_iterator<char>(ifs)),
                      (std::istreambuf_iterator<char>()));
  return content;
}