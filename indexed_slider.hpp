#include <QtWidgets>

namespace kaos {
class indexed_slider : public QSlider {
  Q_OBJECT

  void sliderChange(SliderChange c);

 public:
  int id;

  indexed_slider(int i, QWidget *parent = 0);
  indexed_slider(int i, Qt::Orientation o, QWidget *parent = 0);

 signals:
  void value_changed(int val, int id);
};
};  // namespace kaos
