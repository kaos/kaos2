/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "view_creator.hpp"

#include <iostream>

using namespace std;
using namespace kaos;

interface::view_creator::view_creator(fractal::view_specification s, int nstates, int nparams) {
  setLayout(new QVBoxLayout());

  QGroupBox *hgroup = new QGroupBox("Horizontal Axis", this);
  QVBoxLayout *l = new QVBoxLayout(hgroup);
  l->addWidget(htype = new QCheckBox("show state", this));
  l->addWidget(hdim = new QComboBox(this));
  layout()->addWidget(hgroup);

  QGroupBox *vgroup = new QGroupBox("Vertical Axis", this);
  l = new QVBoxLayout(vgroup);
  l->addWidget(vtype = new QCheckBox("show state", this));
  l->addWidget(vdim = new QComboBox(this));
  layout()->addWidget(vgroup);

  QPushButton *b_accept = new QPushButton("Accept", this);
  layout()->addWidget(b_accept);
  connect(b_accept, SIGNAL(clicked()), this, SLOT(accept()));

  deparse(s, nstates, nparams);

  connect(htype, SIGNAL(toggled(bool)), this, SLOT(set_htype(bool)));
  connect(vtype, SIGNAL(toggled(bool)), this, SLOT(set_vtype(bool)));
}

void interface::view_creator::set_htype(bool s) {
  int hmax = s ? nstates : nparams;
  hdim->clear();
  for (int i = 0; i < hmax; i++) hdim->addItem(QString::number(i));
}

void interface::view_creator::set_vtype(bool s) {
  int vmax = s ? nstates : nparams;
  vdim->clear();
  for (int i = 0; i < vmax; i++) vdim->addItem(QString::number(i));
}

fractal::view_specification interface::view_creator::parse() {
  fractal::view_specification s;

  s.horizontal.first = htype->isChecked() ? fractal::view_state : fractal::view_param;
  s.horizontal.second = hdim->currentIndex();

  s.vertical.first = vtype->isChecked() ? fractal::view_state : fractal::view_param;
  s.vertical.second = vdim->currentIndex();

  return s;
}

void interface::view_creator::deparse(fractal::view_specification s, int ns, int np) {
  bool sp;

  nstates = ns;
  nparams = np;

  sp = s.horizontal.first == fractal::view_state;
  htype->setChecked(sp);
  set_htype(sp);
  hdim->setCurrentIndex(s.horizontal.second);

  sp = s.vertical.first == fractal::view_state;
  vtype->setChecked(sp);
  set_vtype(sp);
  vdim->setCurrentIndex(s.vertical.second);
}
