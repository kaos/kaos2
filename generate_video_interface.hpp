/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#pragma once
#include <QtWidgets>

#include "fractal_lw.hpp"
#include "fractal_specification.hpp"

namespace kaos {
namespace interface {
/*! \brief Dialog class for getting video generation specs from user */
class videox : public QDialog {
  Q_OBJECT
  fractal_lw *input_f_source;
  fractal_lw *input_f_target;
  QLineEdit *input_filename;
  QLineEdit *input_width;
  QLineEdit *input_height;
  QLineEdit *input_seconds;
  QLineEdit *input_fps;
  QCheckBox *input_expslow;

 public:
  /*! Constructor
	@param parent QWidget to pass to #QDialog
      */
  videox(QWidget *parent = 0, bool get_source_target = true);

  /*! get the entered filename */
  QString get_filename();

  /*! get the entered image width */
  int get_width();

  /*! get the entered image height */
  int get_height();

  int get_seconds();
  int get_fps();
  bool get_expslow();

  fractal::specification get_f_source();
  fractal::specification get_f_target();
};
};  // namespace interface
};  // namespace kaos
