#include "system_creator.hpp"

#include <cstdlib>
#include <iostream>

#include "elementary.hpp"

using namespace std;
using namespace kaos;

interface::system_creator::system_creator(fractal::system_specification s) : QDialog() {
  QVBoxLayout *l = new QVBoxLayout(this);

  group_expressions = new QGroupBox("update expressions", this);
  group_expressions->setLayout(new QVBoxLayout());
  group_expressions->setMinimumWidth(500);
  l->addWidget(group_expressions);

  QPushButton *button_add = new QPushButton("add dimension", this);
  connect(button_add, SIGNAL(clicked()), this, SLOT(add_dimension_slot()));
  l->addWidget(button_add);

  QHBoxLayout *lcond = new QHBoxLayout();
  input_condition = new QLineEdit(this);
  lcond->addWidget(new QLabel("condition:"));
  lcond->addWidget(input_condition);
  l->addLayout(lcond);

  QHBoxLayout *lits = new QHBoxLayout();
  input_iterations = new QLineEdit(this);
  lits->addWidget(new QLabel("max iterations:"));
  lits->addWidget(input_iterations);
  l->addLayout(lits);

  input_is_DE = new QCheckBox("Is DE?", this);
  input_is_DE->setCheckState(Qt::Unchecked);
  l->addWidget(input_is_DE);

  QPushButton *b_accept = new QPushButton("Accept", this);
  connect(b_accept, SIGNAL(clicked()), this, SLOT(accept()));
  l->addWidget(b_accept);

  deparse(s);

  layout()->setSizeConstraint(QLayout::SetFixedSize);
};

QSize interface::system_creator::sizeHint() const {
  return layout()->sizeHint();
}

void interface::system_creator::add_dimension_slot() {
  this->add_dimension("");
}

void interface::system_creator::add_dimension(QString x) {
  QGroupBox *box = new QGroupBox();
  QHBoxLayout *l = new QHBoxLayout();
  box->setLayout(l);

  int i = input_expression.size();
  if (input_is_DE->checkState()) {
    l->addWidget(new QLabel(("dx" + to_string(i) + "/dt = ").c_str()));
  } else {
    l->addWidget(new QLabel(("x(" + to_string(i) + ", k+1) = ").c_str()));
  }

  QLineEdit *edit = new QLineEdit(x);
  l->addWidget(edit);
  input_expression.push_back(make_pair(edit, new closable(box, this)));
  group_expressions->layout()->addWidget(input_expression.back().second);
  connect(input_expression.back().second, SIGNAL(was_closed(closable *)), this, SLOT(close_input(closable *)));
}

void interface::system_creator::close_input(closable *x) {
  for (auto y = input_expression.begin(); y != input_expression.end(); y++) {
    if (x == y->second) {
      input_expression.erase(y);
      group_expressions->layout()->removeWidget(x);
      delete x;
      layout()->invalidate();
      adjustSize();
      return;
    }
  }

  cout << "system_creator: close_input: error: widget not in list!" << endl;
  exit(-1);
}

void interface::system_creator::deparse(fractal::system_specification s) {
  // remove old expressions
  for (auto x : input_expression) {
    group_expressions->layout()->removeWidget(x.second);
    delete x.second;
  }
  input_expression.clear();

  // set condition
  input_condition->setText(s.condition);

  // set iterations
  input_iterations->setText(QString::number(s.maximum_iterations));

  // set is DE
  input_is_DE->setCheckState(s.is_DE ? Qt::Checked : Qt::Unchecked);

  // add new expressions
  for (auto x : s.update_expression) add_dimension(x);
}

fractal::system_specification interface::system_creator::parse() {
  fractal::system_specification res;

  for (auto x : input_expression) {
    QLineEdit *q = x.first;
    if (q) {
      res.update_expression.push_back(q2cstring(q->text()));
    } else {
      cout << "system_creator::parse: cast failed!" << endl;
    }
  }
  res.condition = q2cstring(input_condition->text());
  res.maximum_iterations = input_iterations->text().toInt();
  res.is_DE = !!input_is_DE->checkState();

  return res;
}
