/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include <QtWidgets>

#include "closable.hpp"
#include "colormap.hpp"
#include "fractal_specification.hpp"

namespace kaos {
namespace interface {
class color_node : public QWidget {
  Q_OBJECT

  QLineEdit *ivalue;
  QPushButton *icolor;

 public:
  QColor color;
  types::value value;

  class compare {
   public:
    // representing a < b
    bool operator()(color_node *a, color_node *b);
  };

  color_node(types::value v, QColor c, QWidget *parent = 0);

 public slots:
  void set_value(QString);
  void set_color();
};

class interpolation_gui : public QWidget {
  Q_OBJECT

 public:
  QComboBox *select_property;
  QList<color_node *> nodes;
  QList<QCheckBox *> apply_dims;
  QComboBox *merge;  // managed by color_creator

  interpolation_gui(color::interpolation c, QWidget *parent = 0);

 public slots:
  void add_node();
  void close_node(closable *n);
};

class color_creator : public QDialog {
  Q_OBJECT

  int dim;
  QList<interpolation_gui *> maps;

 public:
  color_creator(fractal::color_specification s, int d);
  fractal::color_specification parse();
  void deparse(fractal::color_specification s);
  void add_map(color::interpolation, color::merge_operator);

 public slots:
  void add_map();
  void close_map(closable *cg);
};
};  // namespace interface
};  // namespace kaos
