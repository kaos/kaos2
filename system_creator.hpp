/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#pragma once
#include <QtWidgets>
#include <list>

#include "closable.hpp"
#include "fractal_specification.hpp"

namespace kaos {
namespace interface {
class system_creator : public QDialog {
  Q_OBJECT

  std::list<std::pair<QLineEdit *, closable *> > input_expression;
  QLineEdit *input_condition;
  QLineEdit *input_iterations;
  QGroupBox *group_expressions;
  QCheckBox *input_is_DE;

 public:
  system_creator(fractal::system_specification s);
  fractal::system_specification parse();
  void deparse(fractal::system_specification s);
  QSize sizeHint() const;

 private slots:
  void add_dimension_slot();
  void add_dimension(QString x);
  void close_input(closable *x);
};
};  // namespace interface
};  // namespace kaos
