/* The MIT License (MIT)

Copyright (c) 2014 Ross Linscott

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "bounds_creator.hpp"

using namespace std;
using namespace kaos;

interface::bounds_creator::bounds_creator(fractal::bounds_specification s) : QDialog() {
  QVBoxLayout *l = new QVBoxLayout(this);
  content_layout = new QHBoxLayout();
  bounds_layout = new QVBoxLayout();
  states_layout = new QVBoxLayout();
  params_layout = new QVBoxLayout();

  l->addLayout(content_layout);

  QPushButton *b = new QPushButton("Accept", this);
  connect(b, SIGNAL(clicked()), this, SLOT(accept()));
  l->addWidget(b);

  bounds_layout->addWidget(new QLabel("value limits", this));
  states_layout->addWidget(new QLabel("base states", this));
  params_layout->addWidget(new QLabel("base params", this));

  content_layout->addLayout(bounds_layout);
  content_layout->addLayout(states_layout);
  content_layout->addLayout(params_layout);

  deparse(s);
}

void interface::bounds_creator::deparse(fractal::bounds_specification s) {
  // todo: does deconstructor remove widget from layou and or parent?
  for (auto x : bounds) delete x;
  for (auto x : states) delete x;
  for (auto x : params) delete x;

  bounds.resize(4);
  states.resize(s.base[fractal::view_state].size());
  params.resize(s.base[fractal::view_param].size());

  // setup bounds
  for (int i = 0; i < 4; i++) {
    bounds_layout->addWidget(bounds[i] = new QLineEdit(QString::number(s.bounds[i]), this));
  }

  // setup states
  for (int i = 0; i < states.size(); i++) {
    states_layout->addWidget(states[i] = new QLineEdit(QString::number(s.base[fractal::view_state][i]), this));
  }

  // setup params
  for (int i = 0; i < params.size(); i++) {
    params_layout->addWidget(params[i] = new QLineEdit(QString::number(s.base[fractal::view_param][i]), this));
  }
}

fractal::bounds_specification interface::bounds_creator::parse() {
  fractal::bounds_specification s;

  s.base[fractal::view_state].resize(states.size());
  s.base[fractal::view_param].resize(params.size());

  for (int i = 0; i < 4; i++) {
    s.bounds[i] = bounds[i]->text().toDouble();
  }

  for (int i = 0; i < states.size(); i++) {
    s.base[fractal::view_state][i] = states[i]->text().toDouble();
  }

  for (int i = 0; i < params.size(); i++) {
    s.base[fractal::view_param][i] = params[i]->text().toDouble();
  }

  return s;
}
