#pragma once
#include <vector>
#include <functional>

std::vector<double> operator +(std::vector<double> x, std::vector<double> y) {
  for (int i = 0; i < x.size(); i++) x[i] += y[i];
  return x;
}

std::vector<double> operator *(double a, std::vector<double> x) {
  for (auto &y : x) y*= a;
  return x;
}

std::vector<double> rk4_scheme(std::function<std::vector<double>(std::vector<double>)> f, std::vector<double> x, double dt) {
  std::vector<double> k1 = dt * f(x);
  std::vector<double> k2 = dt * f(x + 0.5 * k1);
  std::vector<double> k3 = dt * f(x + 0.5 * k2);
  std::vector<double> k4 = dt * f(x + k3);
  
  return 1/(double)6 * (k1 + k2 + k3 + k4);
}
